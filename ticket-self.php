<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$_SESSION['user_id']);
  $User = $InstanceCollection->getUserList()[0];

  $Ticket = new Ticket();
  #-----------------------------------------------------------------------------
  # Site logic
  #-----------------------------------------------------------------------------
  $DatabaseHandler = $Database->connect();

  $statement = $DatabaseHandler->prepare(
    'SELECT
            t.ticket_id,
            t.ticket_headline,
            t.ticket_create_date,
            t.ticket_create_time,
            t.ticket_sla,
            ts.ticket_status_name,
            tq.queue_name,
            o.user_name,
            o.user_lastname
        FROM
            ticket_db.tickets t
        JOIN ticket_db.ticket_statuses ts ON
            ts.ticket_status_id = t.ticket_status_id
        JOIN ticket_db.ticket_queues tq ON
            tq.queue_id = t.ticket_queue_id
        LEFT JOIN ticket_db.users o ON
            o.user_id = t.ticket_owner_id
        JOIN ticket_db.users r ON
            r.user_id = t.ticket_requestor_id
        WHERE
            r.user_id = :user_id
        ORDER BY
            t.ticket_id
        DESC');

  $statement->bindValue(':user_id', $User->getID(), PDO::PARAM_INT);
  $statement->execute();

  $htmlElement = null;

  if ($statement->rowCount() > 0)
  {
    while ($data = $statement->fetch(PDO::FETCH_ASSOC))
    {
      $htmlElement .=
        '<tr>
          <td><a href="ticket.php?id='.$data['ticket_id'].'"><i class="fas fa-link"></i> '.$data['ticket_id'].'</a></td>
          <td>'.$data['ticket_headline'].'</td>
          <td>'.$data['ticket_create_date'].' '.$data['ticket_create_time'].'</td>
          <td><span class="SLA SLA-'.ucfirst($data['ticket_sla']).'">'.$data['ticket_sla'].'</span></td>
          <td>'.$data['ticket_status_name'].'</td>
          <td>'.$data['queue_name'].'</td>
          <td>'.$data['user_name'].' '.$data['user_lastname'].'</td>
        </tr>';
    }
  }
  else
  {
    $htmlElement .= '<tr><td colspan="7">Brak danych do wyświetlenia</td></tr>';
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $tTemplate = new TemplateBuilder();
  $tTemplate->prepare('src/templates/user-created-ticket-list.html');
  $tTemplate->bind('{{user-ticket-list}}', $htmlElement);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_TICKET_SELF);
  $mTemplate->bind('{{page-content}}', $tTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
