<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
                $ticket_id = !empty($_GET['id']) ? intval($_GET['id']) : 0;
               $ticket_sla = !empty($_GET['sla']) ? trim($_GET['sla']) : 'all';
             $ticket_queue = !empty($_GET['queue']) ? intval($_GET['queue']) : 0;
             $ticket_owner = !empty($_GET['owner']) ? intval($_GET['owner']) : 0;
         $ticket_requestor = !empty($_GET['requestor']) ? intval($_GET['requestor']) : 0;
            $ticket_status = !empty($_GET['status']) ? intval($_GET['status']) : 0;;
           $ticket_product = !empty($_GET['product']) ? intval($_GET['product']) : 0;
          $ticket_priority = !empty($_GET['priority']) ? intval($_GET['priority']) : 0;
               $ticket_env = !empty($_GET['env']) ? intval($_GET['env']) : 0;
            $ticket_impact = !empty($_GET['impact']) ? intval($_GET['impact']) : 0;
         $ticket_parent_id = !empty($_GET['pid']) ? intval($_GET['pid']) : 0;
       $ticket_external_id = !empty($_GET['eid']) ? intval($_GET['eid']) : 0;
  $ticket_create_date_from = !empty($_GET['df']) ? trim($_GET['df']) : '1900-01-01';
    $ticket_create_date_to = !empty($_GET['dt']) ? trim($_GET['dt']) : date('Y-m-d');

  $searchQueryString = '';

         $ticket_id > 0 ? $searchQueryString .= ' AND t.ticket_id = :ticket_id '                     : $searchQueryString .= ' AND t.ticket_id >= :ticket_id ';
      $ticket_queue > 0 ? $searchQueryString .= ' AND t.ticket_queue_id = :ticket_queue_id '         : $searchQueryString .= ' AND t.ticket_queue_id >= :ticket_queue_id ';
      $ticket_owner > 0 ? $searchQueryString .= ' AND t.ticket_owner_id = :ticket_owner_id '         : $searchQueryString .= ' AND (t.ticket_owner_id >= :ticket_owner_id OR t.ticket_owner_id IS NULL) ';
  $ticket_requestor > 0 ? $searchQueryString .= ' AND t.ticket_requestor_id = :ticket_requestor_id ' : $searchQueryString .= ' AND t.ticket_requestor_id >= :ticket_requestor_id ';
     $ticket_status > 0 ? $searchQueryString .= ' AND t.ticket_status_id = :ticket_status_id '       : $searchQueryString .= ' AND t.ticket_status_id >= :ticket_status_id ';
    $ticket_product > 0 ? $searchQueryString .= ' AND t.ticket_product_id = :ticket_product_id '     : $searchQueryString .= ' AND t.ticket_product_id >= :ticket_product_id ';
   $ticket_priority > 0 ? $searchQueryString .= ' AND t.ticket_priority_id = :ticket_priority_id '   : $searchQueryString .= ' AND t.ticket_priority_id >= :ticket_priority_id ';
        $ticket_env > 0 ? $searchQueryString .= ' AND t.ticket_env_id = :ticket_env_id '             : $searchQueryString .= ' AND t.ticket_env_id >= :ticket_env_id ';
     $ticket_impact > 0 ? $searchQueryString .= ' AND t.ticket_impact_id = :ticket_impact_id '       : $searchQueryString .= ' AND t.ticket_impact_id >= :ticket_impact_id ';
  $ticket_parent_id > 0 ? $searchQueryString .= ' AND t.ticket_parent_id = :ticket_parent_id '       : $searchQueryString .= ' AND t.ticket_parent_id >= :ticket_parent_id ';
$ticket_external_id > 0 ? $searchQueryString .= ' AND t.ticket_external_id = :ticket_external_id '   : $searchQueryString .= ' AND t.ticket_external_id >= :ticket_external_id ';
   $ticket_sla != 'all' ? $searchQueryString .= ' AND t.ticket_sla = :ticket_sla '                   : $searchQueryString .= ' AND t.ticket_sla <> :ticket_sla  ';
                          $searchQueryString .= ' AND t.ticket_create_date >= :df ';
                          $searchQueryString .= ' AND t.ticket_create_date <= :dt ';



  $Ticket = new Ticket();

  $ticket_queues = null;
  $ticket_products = null;
  $ticket_priorities = null;
  $ticket_envs = null;
  $ticket_impacts = null;
  $ticket_statuses = null;
  $ticket_owner_list = null;
  $ticket_requestor_list = null;
  $ticket_sla_list = null;

  foreach ($Ticket->getProductList() as $row) $ticket_products .= '<option value="'.$row['product_id'].'" '.(@$_GET['product'] == $row['product_id'] ? 'selected' : '').'>'.$row['product_name'].'</option>';
  foreach ($Ticket->getPriorityList() as $row) $ticket_priorities .= '<option value="'.$row['priority_id'].'" '.(@$_GET['priority'] == $row['priority_id'] ? 'selected' : '').'>'.$row['priority_name'].'</option>';
  foreach ($Ticket->getEnvList() as $row) $ticket_envs .= '<option value="'.$row['env_id'].'" '.(@$_GET['env'] == $row['env_id'] ? 'selected' : '').'>'.$row['env_name'].'</option>';
  foreach ($Ticket->getImpactList() as $row) $ticket_impacts .= '<option value="'.$row['impact_id'].'" '.(@$_GET['impact'] == $row['impact_id'] ? 'selected' : '').'>'.$row['impact_name'].'</option>';
  foreach ($Ticket->getStatusList() as $row) $ticket_statuses .= '<option value="'.$row['ticket_status_id'].'" '.(@$_GET['status'] == $row['ticket_status_id'] ? 'selected' : '').'>'.$row['ticket_status_name'].'</option>';
  foreach ($Ticket->getFullQueueList() as $row) $ticket_queues .= '<option value="'.$row['queue_id'].'" '.(@$_GET['queue'] == $row['queue_id'] ? 'selected' : '').'>'.$row['queue_name'].'</option>';
  foreach ($Ticket->getFullQueueUserList() as $row) $ticket_owner_list .= '<option value="'.$row['user_id'].'" '.(@$_GET['owner'] == $row['user_id'] ? 'selected' : '').'>'.$row['user_lastname'].' '.$row['user_name'].'</option>';
  foreach ($Ticket->getFullQueueUserList() as $row) $ticket_requestor_list .= '<option value="'.$row['user_id'].'" '.(@$_GET['requestor'] == $row['user_id'] ? 'selected' : '').'>'.$row['user_lastname'].' '.$row['user_name'].'</option>';
  foreach ($Ticket->getSLAList() as $row) $ticket_sla_list .= '<option value="'.$row.'" '.(@$_GET['sla'] == $row ? 'selected' : '').'>'.$row.'</option>';
  #-----------------------------------------------------------------------------
  # Site logic
  #-----------------------------------------------------------------------------
  $htmlElement = null;

  if (!empty($_GET))
  {
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT
              t.ticket_id,
              t.ticket_headline,
              t.ticket_create_date,
              t.ticket_create_time,
              t.ticket_sla,
              ts.ticket_status_name,
              tq.queue_name,
              o.user_name,
              o.user_lastname
          FROM
              ticket_db.tickets t
          LEFT JOIN ticket_db.ticket_statuses ts ON
              ts.ticket_status_id = t.ticket_status_id
          LEFT JOIN ticket_db.ticket_queues tq ON
              tq.queue_id = t.ticket_queue_id
          LEFT JOIN ticket_db.users o ON
              o.user_id = t.ticket_owner_id
          LEFT JOIN ticket_db.users r ON
              r.user_id = t.ticket_requestor_id
          WHERE 1 = 1
              '.$searchQueryString.'
          ORDER BY
              t.ticket_id
          DESC');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_queue_id', $ticket_queue, PDO::PARAM_INT);
    $statement->bindValue(':ticket_owner_id', $ticket_owner, PDO::PARAM_INT);
    $statement->bindValue(':ticket_requestor_id', $ticket_requestor, PDO::PARAM_INT);
    $statement->bindValue(':ticket_status_id', $ticket_status, PDO::PARAM_INT);
    $statement->bindValue(':ticket_product_id', $ticket_product, PDO::PARAM_INT);
    $statement->bindValue(':ticket_priority_id', $ticket_priority, PDO::PARAM_INT);
    $statement->bindValue(':ticket_env_id', $ticket_env, PDO::PARAM_INT);
    $statement->bindValue(':ticket_impact_id', $ticket_impact, PDO::PARAM_INT);
    $statement->bindValue(':ticket_parent_id', $ticket_parent_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_external_id', $ticket_external_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_sla', $ticket_sla, PDO::PARAM_STR);
    $statement->bindValue(':df', $ticket_create_date_from, PDO::PARAM_STR);
    $statement->bindValue(':dt', $ticket_create_date_to, PDO::PARAM_STR);
    $statement->execute();

    $counter = $statement->rowCount();

    if ($statement->rowCount() > 0)
    {
      while ($data = $statement->fetch(PDO::FETCH_ASSOC))
      {
        $htmlElement .=
          '<tr>
            <td><a href="ticket.php?id='.$data['ticket_id'].'"><i class="fas fa-link"></i> '.$data['ticket_id'].'</a></td>
            <td>'.$data['ticket_headline'].'</td>
            <td>'.$data['ticket_create_date'].' '.$data['ticket_create_time'].'</td>
            <td><span class="SLA SLA-'.ucfirst($data['ticket_sla']).'">'.$data['ticket_sla'].'</span></td>
            <td>'.$data['ticket_status_name'].'</td>
            <td>'.$data['queue_name'].'</td>
            <td>'.$data['user_name'].' '.$data['user_lastname'].'</td>
          </tr>';
      }
    }
    else
    {
      $htmlElement .= '<tr><td colspan="7">Brak danych do wyświetlenia</td></tr>';
    }
  }
  else
  {
      $htmlElement = '<tr><td colspan="7">Brak danych do wyświetlenia</td></tr>';
      $counter = 0;
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $tTemplate = new TemplateBuilder();
  $tTemplate->prepare('src/templates/ticket-search.html');
  $tTemplate->bind('{{search-list}}', $htmlElement);
  $tTemplate->bind('{{search-count}}', $counter);
  $tTemplate->bind('{{queue-list}}', $ticket_queues);
  $tTemplate->bind('{{product-list}}', $ticket_products);
  $tTemplate->bind('{{priority-list}}', $ticket_priorities);
  $tTemplate->bind('{{env-list}}', $ticket_envs);
  $tTemplate->bind('{{impact-list}}', $ticket_impacts);
  $tTemplate->bind('{{status-list}}', $ticket_statuses);
  $tTemplate->bind('{{owner-list}}', $ticket_owner_list);
  $tTemplate->bind('{{requestor-list}}', $ticket_requestor_list);
  $tTemplate->bind('{{ticket-id}}', $ticket_id);
  $tTemplate->bind('{{ticket-parent-id}}', $ticket_parent_id);
  $tTemplate->bind('{{ticket-external-id}}', $ticket_external_id);
  $tTemplate->bind('{{ticket-df}}', $ticket_create_date_from);
  $tTemplate->bind('{{ticket-dt}}', $ticket_create_date_to);
  $tTemplate->bind('{{sla-list}}', $ticket_sla_list);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_TICKET_SEARCH);
  $mTemplate->bind('{{page-content}}', $tTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
