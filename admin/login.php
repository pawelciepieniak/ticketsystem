<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == true) $App->redirect('index.php');

  # Logic of site
  if (isset($_POST['admin-login-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $login = trim($_POST['admin-login']);;
    $password = trim($_POST['admin-password']);

    # Check empty fields
    if (empty($login) || empty($password))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is login exists in database
      $statement = $DatabaseHandler->prepare('SELECT admin_id,
                                                     admin_login,
                                                     admin_password
                                                FROM ticket_db.admins
                                               WHERE admin_login = :admin_login');

      $statement->bindValue(':admin_login', $login, PDO::PARAM_STR);
      $rowCount = $statement->execute();

      if ($rowCount == 1)
      {
        $data = $statement->fetch(PDO::FETCH_ASSOC);

        if (password_verify($password, $data['admin_password']))
        {
          $App->createAdminSession($data['admin_id']);
          $App->redirect('index.php');
          exit();
        }
        else
        {
          $MessageCollection->add('<div class="message message-error">Niepoprawny login lub hasło</div>');
        }
      }
    }
  }

  $LocalTemplate = new TemplateBuilder();
  $LocalTemplate->prepare('../src/templates/admin-form-login.html');
  
  if ($MessageCollection->size() > 0)
    $LocalTemplate->bind('{{error}}', $MessageCollection->getValueByIndex(0));
  else
    $LocalTemplate->bind('{{error}}', null);

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_LOGIN);
  $MainTemplate->bind('{{page-content}}', $LocalTemplate->render());

  echo $MainTemplate->render();

?>
