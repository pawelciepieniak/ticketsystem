<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == true)
  {
    $App->destroyAdminSession();
  }

  $App->redirect('login.php');
?>
