<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');

  # Site logic | New
  if (isset($_POST['new-product-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $product_name = trim($_POST['product_name']);

    if (empty($product_name))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      $statement = $DatabaseHandler->prepare('SELECT product_id
                                                FROM ticket_db.ticket_products
                                               WHERE LOWER(product_name) = :product_name');

      $statement->bindValue(':product_name', strtolower($product_name), PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podany system/produkt istnieje już w bazie danych</div>');
      }
      else
      {
        $Product = new Product(
          null,
          $product_name
        );

        $Product->add();
        $App->redirect('products.php');
      }
    }
  }

  # Site logic | Remove
  if (isset($_POST['delete-product-submit']))
  {
    $product_id = $_POST['product_id'];

    $InstanceCollection->setAdditionalParameters('WHERE product_id = '.$product_id);
    $product = $InstanceCollection->getProductList();
    foreach ($product as $object) $object->delete();
    $App->redirect('products.php');
  }

  # Site logic | Update
  if (isset($_POST['edit-product-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $product_id = trim($_POST['product_id']);
    $product_name = trim($_POST['product_name']);
    $product_old_name = trim($_POST['product_name']);

    if (empty($product_id) || empty($product_name))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is user (email address) already exists in database
      $statement = $DatabaseHandler->prepare('SELECT product_id
                                                FROM ticket_db.ticket_products
                                               WHERE LOWER(product_name) = :product_name
                                                AND LOWER(product_name) <> :product_old_name');

      $statement->bindValue(':product_name', $product_name, PDO::PARAM_STR);
      $statement->bindValue(':product_old_name', $product_old_name, PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podany system/produkt istnieje już w bazie danych</div>');
      }
      else
      {
        $Product = new Product(
          $product_id,
          $product_name
        );

        $Product->update();
        $App->redirect('products.php');
      }
    }
  }

  # Prepare data
  $InstanceCollection->setAdditionalParameters('ORDER BY product_name ASC');
  $products = $InstanceCollection->getProductList();
  $productRow = null;

  foreach($products as $object)
  {
    $productRow .= '<tr>';
    $productRow .= '<td class="text-center" name="product-id">'.$object->getID().'</td>';
    $productRow .= '<td name="product-name">'.$object->getName().'</td>';
    $productRow .= '<td class="text-center"><button type="button" class="edit-product-button" data-id="'.$object->getID().'">Edytuj</button></td>';
    $productRow .= '<td class="text-center"><button type="button" class="delete-product-button" data-id="'.$object->getID().'">Usuń</button></td>';
    $productRow .= '<tr>';
  }

  # Prepare view
  $tProducts = new TemplateBuilder();
  $tProducts->prepare('../src/templates/admin-products.html');
  $tProducts->bind('{{product-list}}', $productRow);

  if ($MessageCollection->size() > 0)
    $tProducts->bind('{{errors}}', $MessageCollection->getValueByIndex(0));
  else
    $tProducts->bind('{{errors}}', null);

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_PRODUCTS);
  $MainTemplate->bind('{{main-headline}}', 'Systemy/Produkty');
  $MainTemplate->bind('{{page-content}}', $tProducts->render());

  # Render
  echo $MainTemplate->render();



?>
