<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');

  # Check if queue is no empty
  if (empty($_GET['queue']) || !isset($_GET['queue'])) $App->redirect('queues.php');

  $queue_id = intval($_GET['queue']);


  $InstanceCollection->setAdditionalParameters('WHERE queue_id = '.$queue_id);
  $queueName = $InstanceCollection->getQueuetList()[0]->getName();
  $queueFlow = $InstanceCollection->getQueueFlow($queue_id);
  $queueFlowRow = null;

  //$App->debug($queueFlow);

  # Prepare data
  foreach($queueFlow as $flow)
  {
    $queueFlowRow .= '<tr>';
    $queueFlowRow .= '<td>'.$queueName.'</td>';
    $queueFlowRow .= '<td name="queue-id" data-id="'.$flow['queue_id'].'">'.$flow['queue_name'].'</td>';
    $queueFlowRow .= '<td class="text-center">
                          <input name="flow-queue-id[]" type="checkbox" value="'.$flow['queue_id'].'" '.($flow['target_queue_id'] != null ? 'checked' : '').' />
                     </td>';
    $queueFlowRow .= '</tr>';
  }

  # Prepare view
  $tFlow = new TemplateBuilder();
  $tFlow->prepare('../src/templates/admin-queue-flow.html');
  $tFlow->bind('{{flow-list}}', $queueFlowRow);

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_QUEUE_FLOW);
  $MainTemplate->bind('{{main-headline}}', 'Procesflow kolejki: '.$queueName);
  $MainTemplate->bind('{{page-content}}', $tFlow->render());

  # Render
  echo $MainTemplate->render();

  # Site Logic
  if (isset($_POST['queue-flow-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('DELETE FROM ticket_db.queues_flow WHERE queue_id = :queue_id');
    $statement->bindValue(':queue_id', $queue_id, PDO::PARAM_INT);
    $statement->execute();
    $statement = null;

    $queueFlowArray = $_POST['flow-queue-id'];
    if (count($queueFlowArray)  > 0)
    {
      $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.queues_flow VALUES (:queue_id, :queue_target_id)');
      $statement->bindValue(':queue_id', intval($queue_id), PDO::PARAM_INT);

      foreach ($queueFlowArray as $queue_target_id)
      {
        $statement->bindValue(':queue_target_id', intval($queue_target_id), PDO::PARAM_INT);
        $statement->execute();
      }
    }

    $App->redirect('queue-processflow.php?queue='.$queue_id);
  }
?>
