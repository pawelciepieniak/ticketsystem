<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');


  $iTemplate = new TemplateBuilder();
  $iTemplate->prepare('../src/templates/admin-index.html');

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_INDEX);
  $MainTemplate->bind('{{main-headline}}', 'Strona główna');
  $MainTemplate->bind('{{page-content}}', $iTemplate->render());

  echo $MainTemplate->render();
?>
