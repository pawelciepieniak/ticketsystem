<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');

  # Site logic | New user
  if (isset($_POST['new-user-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $user_name = trim($_POST['user_name']);
    $user_lastname = trim($_POST['user_lastname']);
    $user_email = trim($_POST['user_email']);
    $user_phone_number = trim($_POST['user_phone_number']);
    $user_account_status = trim($_POST['user_account_status']);
    $user_queue = trim($_POST['user_queue']);

    if (empty($user_name) ||
        empty($user_lastname) ||
        empty($user_email) ||
        empty($user_phone_number) ||
        !isset($user_account_status) ||
        !isset($user_queue)
    )
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is user (email address) already exists in database
      $statement = $DatabaseHandler->prepare('SELECT user_id
                                                FROM ticket_db.users
                                               WHERE user_email = :user_email');

      $statement->bindValue(':user_email', $user_email, PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podany adres email istnieje już w bazie danych</div>');
      }
      else
      {
        $password = 'parole1';

        # Send an email
        if ($app_config['email_sender_service'] == 1)
        {
          $password = $App->generatePassword();

          $EmailSender->setRecipient($user_email);
          $EmailSender->setSubject('Ticket System | Pomyślne stworzenie konta');
          $EmailSender->setHeaders();
          $EmailSender->prepare('../src/templates/emails/new-user.html');
          $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
          $EmailSender->bind('{{user_name}}', $user_name);
          $EmailSender->bind('{{user_lastname}}', $user_lastname);
          $EmailSender->bind('{{user_email}}', $user_email);
          $EmailSender->bind('{{user_password}}', $password);
          $EmailSender->setMessage($EmailSender->render());
          $EmailSender->send();
        }

        $User = new User(
          null,
          $user_name,
          $user_lastname,
          $user_email,
          password_hash($password, PASSWORD_DEFAULT),
          $user_phone_number,
          $user_account_status,
          $user_queue,
          null,
          date('Y-m-d H:i:s'),
          null
        );
        $User->add();
        $App->redirect('users.php');
      }
    }
  }

  # Site logic | Remove user
  if (isset($_POST['delete-user-submit']))
  {
    $user_id = $_POST['user_id'];

    $DatabaseHandler = $Database->connect();
    $statement = $DatabaseHandler->prepare('
      SELECT
          COUNT(t.ticket_id) AS ticket_count
      FROM
          ticket_db.tickets t
      #LEFT JOIN ticket_db.users u ON
      #    u.user_queue_id = t.ticket_queue_id
      WHERE
        t.ticket_owner_id = :user_id
      #    u.user_id = :user_id');

    $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $statement->execute();
    $fetcheData = $statement->fetch(PDO::FETCH_ASSOC);

    if (intval($fetcheData['ticket_count']) > 0 || intval($fetcheData['user_count']) > 0)
    {
      $MessageCollection->add('<div class="message message-error">Nie możesz usunąć użytkownika, który ma przypisane do siebie zgłoszenia ('.$fetcheData['ticket_count'].')</div>');
    }
    else
    {
      $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$user_id);
      $user = $InstanceCollection->getUserList();
      foreach ($user as $object) $object->delete();
      $App->redirect('users.php');
    }
  }

  # Site logic | Update user
  if (isset($_POST['edit-user-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $user_id = trim($_POST['user_id']);
    $user_name = trim($_POST['user_name']);
    $user_lastname = trim($_POST['user_lastname']);
    $user_email = trim($_POST['user_email']);
    $user_old_email = trim($_POST['user_old_email']);
    $user_phone_number = trim($_POST['user_phone_number']);
    $user_account_status = trim($_POST['user_account_status']);
    $user_queue = trim($_POST['user_queue']);

    if (empty($user_id) ||
        empty($user_name) ||
        empty($user_lastname) ||
        empty($user_email) ||
        empty($user_phone_number) ||
        !isset($user_account_status) ||
        !isset($user_queue)
    )
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is user (email address) already exists in database
      $statement = $DatabaseHandler->prepare('SELECT user_id
                                                FROM ticket_db.users
                                               WHERE user_email = :user_email
                                                 AND user_email <> :user_old_email');

      $statement->bindValue(':user_email', $user_email, PDO::PARAM_STR);
      $statement->bindValue(':user_old_email', $user_old_email, PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podany adres email istnieje już w bazie danych</div>');
      }
      else
      {
        $User = new User(
          $user_id,
          $user_name,
          $user_lastname,
          $user_email,
          null,
          $user_phone_number,
          $user_account_status,
          $user_queue,
          null,
          null,
          null
        );

        $User->update();
        $App->redirect('users.php');
      }
    }
  }

  # Prepare data
  $InstanceCollection->setAdditionalParameters('ORDER BY queue_id ASC');
  $queues = $InstanceCollection->getQueuetList();
  $queueSelect = null;

  $InstanceCollection->setAdditionalParameters('ORDER BY user_lastname ASC, user_name ASC');
  $users = $InstanceCollection->getUserList();
  $userRow = null;

  foreach ($queues as $object)
  {
    $queueSelect .= '<option value="'.$object->getId().'">'.$object->getName().'</option>';
  }

  foreach($users as $object)
  {
    $userRow .= '<tr>';
    $userRow .= '<td class="text-center" name="user-id">'.$object->getID().'</td>';
    $userRow .= '<td name="user-name">'.$object->getName().'</td>';
    $userRow .= '<td name="user-lastname">'.$object->getLastname().'</td>';
    $userRow .= '<td name="user-email">'.$object->getEmail().'</td>';
    $userRow .= '<td name="user-phone-number">'.$object->getPhoneNumber().'</td>';
    $userRow .= '<td name="user-account-status" data-id="'.$object->getAccountStatus().'">'.$object->getAccountStatusName().'</td>';
    $userRow .= '<td name="user-queue-id" data-id="'.$object->getQueueID().'">'.$object->getQueueName().'</td>';
    $userRow .= '<td name="user-reg-date">'.$object->getRegDate().'</td>';
    $userRow .= '<td name="user-last-online-date">'.$object->getLastOnlineDate().'</td>';
    $userRow .= '<td class="text-center"><button type="button" class="edit-user-button" data-id="'.$object->getID().'">Edytuj</button></td>';
    $userRow .= '<td class="text-center"><button type="button" class="delete-user-button" data-id="'.$object->getID().'">Usuń</button></td>';
    $userRow .= '<tr>';
  }

  # Prepare view
  $tUsers = new TemplateBuilder();
  $tUsers->prepare('../src/templates/admin-users.html');
  $tUsers->bind('{{queue-list}}', $queueSelect);
  $tUsers->bind('{{users-list}}', $userRow);

  if ($MessageCollection->size() > 0)
    $tUsers->bind('{{errors}}', $MessageCollection->getValueByIndex(0));
  else
    $tUsers->bind('{{errors}}', null);

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_USERS);
  $MainTemplate->bind('{{main-headline}}', 'Użytkownicy');
  $MainTemplate->bind('{{page-content}}', $tUsers->render());

  # Render
  echo $MainTemplate->render();



?>
