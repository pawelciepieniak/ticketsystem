<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');

  # Site logic | New
  if (isset($_POST['new-queue-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $queue_name = trim($_POST['queue_name']);

    if (empty($queue_name))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      $statement = $DatabaseHandler->prepare('SELECT queue_id
                                                FROM ticket_db.ticket_queues
                                               WHERE LOWER(queue_name) = :queue_name');

      $statement->bindValue(':queue_name', strtolower($queue_name), PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podana kolejka istnieje już w bazie danych</div>');
      }
      else
      {
        $queue = new queue(
          null,
          $queue_name
        );

        $queue->add();
        $App->redirect('queues.php');
      }
    }
  }

  # Site logic | Remove
  if (isset($_POST['delete-queue-submit']))
  {
    $queue_id = $_POST['queue_id'];

    $DatabaseHandler = $Database->connect();
    $statement = $DatabaseHandler->prepare('
         SELECT
      	        COUNT(DISTINCT t.ticket_id) AS ticket_count,
      	        COUNT(DISTINCT u.user_id) AS user_count
           FROM ticket_db.ticket_queues q
      LEFT JOIN ticket_db.tickets t
             ON t.ticket_queue_id = q.queue_id
      LEFT JOIN ticket_db.users u
            ON u.user_queue_id = q.queue_id
         WHERE q.queue_id = :queue_id');

    $statement->bindValue(':queue_id', $queue_id, PDO::PARAM_INT);
    $statement->execute();
    $fetcheData = $statement->fetch(PDO::FETCH_ASSOC);

    if (intval($fetcheData['ticket_count']) > 0 || intval($fetcheData['user_count']) > 0)
    {
      $MessageCollection->add('<div class="message message-error">Nie możesz usunąć kolejki ponieważ są do niej przypisani użytkownicy ('.$fetcheData['user_count'].') lub zgłoszenia ('.$fetcheData['ticket_count'].')</div>');
    }
    else
    {
      $InstanceCollection->setAdditionalParameters('WHERE queue_id = '.$queue_id);
      $queue = $InstanceCollection->getQueuetList();
      foreach ($queue as $object) $object->delete();
      $App->redirect('queues.php');
    }
  }

  # Site logic | Update
  if (isset($_POST['edit-queue-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $queue_id = trim($_POST['queue_id']);
    $queue_name = trim($_POST['queue_name']);
    $queue_old_name = trim($_POST['queue_name']);

    if (empty($queue_id) || empty($queue_name))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is user (email address) already exists in database
      $statement = $DatabaseHandler->prepare('SELECT queue_id
                                                FROM ticket_db.ticket_queues
                                               WHERE LOWER(queue_name) = :queue_name
                                                AND LOWER(queue_name) <> :queue_old_name');

      $statement->bindValue(':queue_name', $queue_name, PDO::PARAM_STR);
      $statement->bindValue(':queue_old_name', $queue_old_name, PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $MessageCollection->add('<div class="message message-error">Podana kolejka istnieje już w bazie danych</div>');
      }
      else
      {
        $Queue = new queue(
          $queue_id,
          $queue_name
        );

        $Queue->update();
        $App->redirect('queues.php');
      }
    }
  }

  # Prepare data
  $InstanceCollection->setAdditionalParameters('ORDER BY queue_name ASC');
  $queues = $InstanceCollection->getQueuetList();
  $queueRow = null;

  foreach($queues as $object)
  {
    $queueRow .= '<tr>';
    $queueRow .= '<td class="text-center" name="queue-id">'.$object->getID().'</td>';
    $queueRow .= '<td name="queue-name">'.$object->getName().'</td>';
    $queueRow .= '<td class="text-center" name="queue-processflow"><a href="queue-processflow.php?queue='.$object->getID().'">Ustaw</a></td>';
    $queueRow .= '<td class="text-center"><button type="button" class="edit-queue-button" data-id="'.$object->getID().'">Edytuj</button></td>';
    $queueRow .= '<td class="text-center"><button type="button" class="delete-queue-button" data-id="'.$object->getID().'">Usuń</button></td>';
    $queueRow .= '<tr>';
  }

  # Prepare view
  $tQueue = new TemplateBuilder();
  $tQueue->prepare('../src/templates/admin-queues.html');
  $tQueue->bind('{{queue-list}}', $queueRow);

  if ($MessageCollection->size() > 0)
    $tQueue->bind('{{errors}}', $MessageCollection->getValueByIndex(0));
  else
    $tQueue->bind('{{errors}}', null);

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_QUEUES);
  $MainTemplate->bind('{{main-headline}}', 'Kolejki');
  $MainTemplate->bind('{{page-content}}', $tQueue->render());

  # Render
  echo $MainTemplate->render();



?>
