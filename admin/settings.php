<?php
  require_once '../src/includes/config-admin.php';

  # Check if user is login
  if ($App->checkAdminSession() == false) $App->redirect('login.php');

  # Site logic | save changes
  if (isset($_POST['config-submit']))
  {
    $email_sender_service = intval($_POST['conf_emails']);
    $system_active = intval($_POST['conf_sys_active']);

    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      UPDATE ticket_db.app_config
      SET
        email_sender_service = :email_sender_service,
        system_active = :system_active
    ');

    $statement->bindValue(':email_sender_service', $email_sender_service, PDO::PARAM_INT);
    $statement->bindValue(':system_active', $system_active, PDO::PARAM_INT);

    $statement->execute();
    $App->redirect('settings.php');
  }

  # Prepare view
  $tConfig = new TemplateBuilder();
  $tConfig->prepare('../src/templates/admin-config.html');

  # Cofnig | Email service
  if ($app_config['email_sender_service'] == 1)
  {
    $tConfig->bind('{{conf_email_0}}', null);
    $tConfig->bind('{{conf_email_1}}', 'selected');
  }
  else
  {
    $tConfig->bind('{{conf_email_0}}', 'selected');
    $tConfig->bind('{{conf_email_1}}', null);
  }

  # Cofnig | System active
  if ($app_config['system_active'] == 1)
  {
    $tConfig->bind('{{conf_sys_active_0}}', null);
    $tConfig->bind('{{conf_sys_active_1}}', 'selected');
  }
  else
  {
    $tConfig->bind('{{conf_sys_active_0}}', 'selected');
    $tConfig->bind('{{conf_sys_active_1}}', null);
  }

  $MainTemplate->bind('{{page-title}}', PAGE_TITLE_CONFIG);
  $MainTemplate->bind('{{main-headline}}', 'Ustawienia');
  $MainTemplate->bind('{{page-content}}', $tConfig->render());

  # Render
  echo $MainTemplate->render();
?>
