<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$_SESSION['user_id']);
  $User = $InstanceCollection->getUserList()[0];

  $Ticket = new Ticket();

  if (!isset($_GET['id']) || $Ticket->ticketExists($_GET['id']) == false) $App->redirect('ticket-new.php');

  $ticket_id = intval($_GET['id']);

  $mainArray = $Ticket->loadPreview($ticket_id, $User->getID());

  $attachments = null;
  if (count($mainArray['attachments']) > 0)
  {
    for ($i = 0; $i < count($mainArray['attachments']); $i++)
    {
      $filename = explode('/', $mainArray['attachments'][$i]['attachment_source_path']);
      $filename = $filename[count($filename)-1];

      $attachments .= '<tr><td><a title="Pobierz plik" href="download.php?file='.urlencode($mainArray['attachments'][$i]['attachment_source_path']).'"><i class="fas fa-file-download"></i></a></td><td>'.$filename.'</td><td>'.$mainArray['attachments'][$i]['attachment_mime_type'].'</td><td>'.$mainArray['attachments'][$i]['user_name'].' '.$mainArray['attachments'][$i]['user_lastname'].'</td><td>'.$mainArray['attachments'][$i]['attachment_upload_date'].'</td></tr>';
    }
  }

  $events = null;
  if (count($mainArray['events']) > 0)
  {
    for ($i = 0; $i < count($mainArray['events']); $i++)
    {
      $events .= '<tr><td>'.(count($mainArray['events']) - $i).'</td><td>'.$mainArray['events'][$i]['event_type'].'</td><td>'.$mainArray['events'][$i]['user_name'].' '.$mainArray['events'][$i]['user_lastname'].'</td><td>'.$mainArray['events'][$i]['event_date'].'</td><td>'.$mainArray['events'][$i]['event_details'].'</td></tr>';
    }
  }

  $subscribers = null;
  if (count($mainArray['subscribers']) > 0)
  {
    for ($i = 0; $i < count($mainArray['subscribers']); $i++)
    {
      $subscribers .= '<tr><td class="text-center"><input name="delete-subscriber[]" type="checkbox" value="'.$mainArray['subscribers'][$i]['user_id'].'"/></td><td>'.$mainArray['subscribers'][$i]['user_name'].'</td><td>'.$mainArray['subscribers'][$i]['user_lastname'].'</td><td>'.$mainArray['subscribers'][$i]['user_email'].'</td><td>'.$mainArray['subscribers'][$i]['user_phone_number'].'</td></tr>';
    }
  }

  $time = null;
  if (count($mainArray['time']) > 0)
  {
    for ($i = 0; $i < count($mainArray['time']); $i++)
    {
      $time .= '<tr><td>'.(count($mainArray['time']) - $i).'</td><td>'.$mainArray['time'][$i]['user_name'].'</td><td>'.$mainArray['time'][$i]['user_lastname'].'</td><td>'.$mainArray['time'][$i]['event_date'].'</td><td>'.$mainArray['time'][$i]['time_spend'].'</td></tr>';
    }
  }

  $comments = null;
  if (count($mainArray['comments']) > 0)
  {
    for ($i = 0; $i < count($mainArray['comments']); $i++)
    {
      $comments .= '<p class="ticket-answer"><span><b>'.$mainArray['comments'][$i]['user_name'].' '.$mainArray['comments'][$i]['user_lastname'].'</b> <i>'.$mainArray['comments'][$i]['comment_create_date'].'</i></span>'.$mainArray['comments'][$i]['comment_description'].'</p>';
    }
  }

  $ticket_queues = null;
  $ticket_products = null;
  $ticket_statuses = null;
  $ticket_priorities = null;
  $ticket_envs = null;
  $ticket_impacts = null;
  $ticket_subscribers = null;
  $ticket_queue_users = null;

  foreach ($Ticket->getQueueList($User->getQueueID()) as $row) $ticket_queues .= '<option value="'.$row['queue_id'].'" '.($mainArray['ticket']['ticket_queue_id'] == $row['queue_id'] ? 'selected' : '').'>'.$row['queue_name'].'</option>';
  foreach ($Ticket->getProductList() as $row) $ticket_products .= '<option value="'.$row['product_id'].'" '.($mainArray['ticket']['ticket_product_id'] == $row['product_id'] ? 'selected' : '').'>'.$row['product_name'].'</option>';
  foreach ($Ticket->getPriorityList() as $row) $ticket_priorities .= '<option value="'.$row['priority_id'].'" '.($mainArray['ticket']['ticket_priority_id'] == $row['priority_id'] ? 'selected' : '').'>'.$row['priority_name'].'</option>';
  foreach ($Ticket->getEnvList() as $row) $ticket_envs .= '<option value="'.$row['env_id'].'" '.($mainArray['ticket']['ticket_env_id'] == $row['env_id'] ? 'selected' : '').'>'.$row['env_name'].'</option>';
  foreach ($Ticket->getImpactList() as $row) $ticket_impacts .= '<option value="'.$row['impact_id'].'" '.($mainArray['ticket']['ticket_impact_id'] == $row['impact_id'] ? 'selected' : '').'>'.$row['impact_name'].'</option>';
  foreach ($Ticket->getSubscriberList($User->getQueueID(), $User->getID()) as $row) $ticket_subscribers .= '<option value="'.$row['user_id'].'">'.$row['user_name'].' '.$row['user_lastname'].' ('.$row['user_email'].')</option>';
  foreach ($Ticket->getStatusList() as $row) $ticket_statuses .= '<option value="'.$row['ticket_status_id'].'" '.($mainArray['ticket']['ticket_status_id'] == $row['ticket_status_id'] ? 'selected' : '').'>'.$row['ticket_status_name'].'</option>';
  foreach ($Ticket->getQueueUserList($mainArray['ticket']['ticket_queue_id']) as $row) $ticket_queue_users .= '<option value="'.$row['user_id'].'" '.($mainArray['ticket']['owner_id'] == $row['user_id'] ? 'selected' : '').'>'.$row['user_lastname'].' '.$row['user_name'].'</option>';

  $ticket_toolbar = null;
  if ($mainArray['ticket']['ticket_queue_id'] == $User->getQueueID())
  {
    $ticket_toolbar = 'block';
  }
  else
  {
    $ticket_toolbar = 'none';
  }

  $ticket_reopen_toolbar = null;
  if ($mainArray['ticket']['ticket_status_id'] == 5 && $User->getQueueID() == 1)
  {
    $ticket_reopen_toolbar = 'block';
  }
  else
  {
    $ticket_reopen_toolbar = 'none';
  }
  #-----------------------------------------------------------------------------
  # Site logic
  #-----------------------------------------------------------------------------
  if (isset($_POST['ticket-reopen-submit']))
  {
    $reopened_ticket_id = intval($_POST['ticket-id']);

    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('UPDATE ticket_db.tickets SET ticket_status_id = 1, ticket_owner_id = null WHERE ticket_id = :ticket_id');
    $statement->bindValue(':ticket_id', $reopened_ticket_id, PDO::PARAM_STR);
    $statement->execute();

    $App->redirect('ticket.php?id='.$ticket_id);
  }

  if (isset($_POST['ticket-time-submit']))
  {
    $time_amount = intval($_POST['ticket-time-amount']);

    if ($time_amount > 0)
    {
      $Ticket->addTime($User->getID(), $ticket_id, $time_amount);
      $App->redirect('ticket.php?id='.$ticket_id);
    }
  }

  if (isset($_POST['new-subscriber-submit']))
  {
    $subscriber = trim($_POST['new-subscriber']);

    if ($Ticket->checkSubscriber($ticket_id, $subscriber) == false)
    {
      $Ticket->addSubscriber($ticket_id, $User->getID(), $subscriber);
      $App->redirect('ticket.php?id='.$ticket_id);
    }
  }

  if (isset($_POST['delete-subscriber-submit']))
  {
    foreach ($_POST['delete-subscriber'] as $sub)
    {
      $Ticket->removeSubscriber($ticket_id, $sub);
    }
    $App->redirect('ticket.php?id='.$ticket_id);
  }

  # Upload and save attachments
  if (isset($_POST['ticket-reply-submit']))
  {
    $reply_comment = trim($_POST['ticket-reply-text']);
    $reply_attachments = @$_FILES['ticket-reply-attachments'];

    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.ticket_comments VALUES(:comment_id, :comment_ticket_id, :comment_description, :comment_author_id, :comment_create_date)');
    $statement->bindValue(':comment_id', null, PDO::PARAM_STR);
    $statement->bindValue(':comment_ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':comment_description', $reply_comment, PDO::PARAM_STR);
    $statement->bindValue(':comment_author_id', $User->getID(), PDO::PARAM_INT);
    $statement->bindValue(':comment_create_date', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $statement->execute();

    $ticket_comment_id = $DatabaseHandler->lastInsertId();

    $count_of_attachments = count(array_filter($reply_attachments['name']));

    if (count($count_of_attachments) > 0)
    {
      $target_dir = ATTACHMENT_TARGET_DIR;

      for ($i = 0; $i < $count_of_attachments; $i++)
      {
        $uploadOk = 1;
        $target_file = $target_dir.$ticket_id.'/'.basename($reply_attachments['name'][$i]);

        $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if (file_exists($target_file))
        {
            $MessageCollection->add('Plik '.basename($reply_attachments['name'][$i]).' już istnieje, prosimy zmień nazwę pliku.');
            $uploadOk = 0;
        }

        if ($reply_attachments['size'][$i] > $App->file_upload_max_size())
        {
            $MessageCollection->add('Plik: '.basename($reply_attachments['name'][$i]).' zbyt duży ('.$reply_attachments['size'].'). Maksymalny rozmiar pliku wynosi '.$App->file_upload_max_size());
            $uploadOk = 0;
        }

        if ($uploadOk == 1)
        {
          if (!move_uploaded_file($reply_attachments['tmp_name'][$i], $target_file))
          {
            $MessageCollection->add('Wystąpił błąd podczas wgrywania pliku: '.basename($reply_attachments['name'][$i]));
            $App->debug($reply_attachments['error'][$i]);
          }
          else
          {
            $attachment_id = null;
            $attachment_ticket_id = $ticket_id;
            $attachment_comment_id = $ticket_comment_id;
            $attachment_upload_date = date('Y-m-d H:i:s');
            $attachment_mime_type = $reply_attachments['type'][$i];
            $attachment_source_path = $target_file;
            $attachment_owner_id = $User->getID();

            $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.ticket_attachments VALUES(:id, :ticket_id, :comment_id, :upload_date, :mime_type, :source_path, :owner_id)');
            $statement->bindValue(':id', $attachment_id, PDO::PARAM_STR);
            $statement->bindValue(':ticket_id', $attachment_ticket_id, PDO::PARAM_INT);
            $statement->bindValue(':comment_id', $attachment_comment_id, PDO::PARAM_STR);
            $statement->bindValue(':upload_date', $attachment_upload_date, PDO::PARAM_STR);
            $statement->bindValue(':mime_type', $attachment_mime_type, PDO::PARAM_STR);
            $statement->bindValue(':source_path', $attachment_source_path, PDO::PARAM_STR);
            $statement->bindValue(':owner_id', $attachment_owner_id, PDO::PARAM_INT);
            $statement->execute();
          }
        }
      }
    }

    # Send email
    if ($app_config['email_sender_service'] == 1)
    {
      $EmailSender->setRecipient($mainArray['ticket']['requestor_email']);
      $EmailSender->setSubject('Ticket System | Do Twojego zgłoszenia został dodany komentarz');
      $EmailSender->setHeaders();
      $EmailSender->prepare('src/templates/emails/reply-ticket.html');
      $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
      $EmailSender->bind('{{ticket-id}}', $ticket_id);
      $EmailSender->bind('{{ticket-reply}}', $reply_comment);
      $EmailSender->setMessage($EmailSender->render());
      $EmailSender->send();

      $EmailSender->setRecipient($mainArray['ticket']['owner_email']);
      $EmailSender->setSubject('Ticket System | Do Twojego zgłoszenia został dodany komentarz');
      $EmailSender->setHeaders();
      $EmailSender->prepare('src/templates/emails/reply-ticket.html');
      $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
      $EmailSender->bind('{{ticket-id}}', $ticket_id);
      $EmailSender->bind('{{ticket-reply}}', $reply_comment);
      $EmailSender->setMessage($EmailSender->render());
      $EmailSender->send();
    }

    if ($MessageCollection->size() > 0)
    {
      $App->debug($MessageCollection);
    }
    else
    {
      $App->redirect('ticket.php?id='.$ticket_id);
    }
  }

  if (isset($_POST['ticket_edit_submit']))
  {
    $ticket_product = intval($_POST['ticket_product']);
    $ticket_status = intval($_POST['ticket_status']);
    $ticket_status_old = intval($_POST['ticket_status_old']);
    $ticket_env = intval($_POST['ticket_env']);
    $ticket_owner = intval($_POST['ticket_owner']);
    $ticket_priority = intval($_POST['ticket_priority']);
    $ticket_parent_id = intval($_POST['ticket_parent_id']);
    $ticket_queue = intval($_POST['ticket_queue']);
    $ticket_impact = intval($_POST['ticket_impact']);
    $ticket_external_id = intval($_POST['ticket_external_id']);
    $ticket_resolve_date = null;
    $ticket_resolve_time = null;

    $ticket_sla = $Ticket->getSLA($ticket_env, $ticket_impact, $ticket_priority);

    if (($ticket_status == 5 || $ticket_status == 7) && ($ticket_status_old != 5 || $ticket_status_old != 6 || $ticket_status_old != 7))
    {
      $ticket_resolve_date = date('Y-m-d');
      $ticket_resolve_time = date('H:i:s');
    }

    $DatabaseHandler = $Database->connect();
    $statement = $DatabaseHandler->prepare('UPDATE ticket_db.tickets
                                               SET
                                                     ticket_status_id = :ticket_status_id,
                                                           ticket_sla = :ticket_sla,
                                                      ticket_queue_id = :ticket_queue_id,
                                                      ticket_owner_id = :ticket_owner_id,
                                                    ticket_product_id = :ticket_product_id,
                                                        ticket_env_id = :ticket_env_id,
                                                   ticket_priority_id = :ticket_priority_id,
                                                     ticket_impact_id = :ticket_impact_id,
                                                     ticket_parent_id = :ticket_parent_id,
                                                   ticket_external_id = :ticket_external_id,
                                                  ticket_resolve_date = :ticket_resolve_date,
                                                  ticket_resolve_time = :ticket_resolve_time
                                                WHERE
                                                    ticket_id = :ticket_id');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_sla', $ticket_sla, PDO::PARAM_STR);
    $statement->bindValue(':ticket_status_id', $ticket_status, PDO::PARAM_INT);
    $statement->bindValue(':ticket_queue_id', $ticket_queue, PDO::PARAM_INT);
    $statement->bindValue(':ticket_owner_id', $ticket_owner, PDO::PARAM_INT);
    $statement->bindValue(':ticket_product_id', $ticket_product, PDO::PARAM_INT);
    $statement->bindValue(':ticket_env_id', $ticket_env, PDO::PARAM_INT);
    $statement->bindValue(':ticket_priority_id', $ticket_priority, PDO::PARAM_INT);
    $statement->bindValue(':ticket_impact_id', $ticket_impact, PDO::PARAM_INT);
    $statement->bindValue(':ticket_parent_id', $ticket_parent_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_external_id', $ticket_external_id, PDO::PARAM_INT);
    $statement->bindValue(':ticket_resolve_date', $ticket_resolve_date, PDO::PARAM_STR);
    $statement->bindValue(':ticket_resolve_time', $ticket_resolve_time, PDO::PARAM_STR);
    $statement->execute();

    # Send email
    if ($ticket_status != $ticket_status_old)
    {
      # Send email
      if ($app_config['email_sender_service'] == 1)
      {
        $EmailSender->setRecipient($mainArray['ticket']['requestor_email']);
        $EmailSender->setSubject('Ticket System | Status Twojego zgłoszenia został zmieniony');
        $EmailSender->setHeaders();
        $EmailSender->prepare('src/templates/emails/change-status-ticket.html');
        $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
        $EmailSender->bind('{{ticket-id}}', $ticket_id);
        $EmailSender->bind('{{ticket-old-status}}', $Ticket->getStatusNameByID($ticket_status_old));
        $EmailSender->bind('{{ticket-new-status}}', $Ticket->getStatusNameByID($ticket_status));
        $EmailSender->setMessage($EmailSender->render());
        $EmailSender->send();

        $EmailSender->setRecipient($mainArray['ticket']['owner_email']);
        $EmailSender->setSubject('Ticket System | Status Twojego zgłoszenia został zmieniony');
        $EmailSender->setHeaders();
        $EmailSender->prepare('src/templates/emails/change-status-ticket.html');
        $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
        $EmailSender->bind('{{ticket-id}}', $ticket_id);
        $EmailSender->bind('{{ticket-old-status}}', $Ticket->getStatusNameByID($ticket_status_old));
        $EmailSender->bind('{{ticket-new-status}}', $Ticket->getStatusNameByID($ticket_status));
        $EmailSender->setMessage($EmailSender->render());
        $EmailSender->send();
      }
    }

    $App->redirect('ticket.php?id='.$ticket_id);
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $tTemplate = new TemplateBuilder();
  $tTemplate->prepare('src/templates/ticket-preview.html');
  $tTemplate->bind('{{ticket-id}}', $ticket_id);
  $tTemplate->bind('{{ticket-headline}}', $mainArray['ticket']['ticket_headline']);
  $tTemplate->bind('{{ticket-requestor-name}}', $mainArray['ticket']['requestor_name']);
  $tTemplate->bind('{{ticket-requestor-lastname}}', $mainArray['ticket']['requestor_lastname']);
  $tTemplate->bind('{{ticket-requestor-email}}', $mainArray['ticket']['requestor_email']);
  $tTemplate->bind('{{ticket-requestor-phone}}', $mainArray['ticket']['requestor_phone_number']);
  $tTemplate->bind('{{ticket-owner-name}}', $mainArray['ticket']['owner_name']);
  $tTemplate->bind('{{ticket-owner-lastname}}', $mainArray['ticket']['owner_lastname']);
  $tTemplate->bind('{{ticket-owner-email}}', $mainArray['ticket']['owner_email']);
  $tTemplate->bind('{{ticket-owner-phone}}', $mainArray['ticket']['owner_phone_number']);
  $tTemplate->bind('{{ticket-description}}', $mainArray['ticket']['ticket_description']);
  $tTemplate->bind('{{ticket-sla}}', $mainArray['ticket']['ticket_sla']);
  $tTemplate->bind('{{ticket-sla-uppercase}}', ucfirst($mainArray['ticket']['ticket_sla']));
  $tTemplate->bind('{{ticket-create-date}}', $mainArray['ticket']['ticket_create_date'].' '.$mainArray['ticket']['ticket_create_time']);
  $tTemplate->bind('{{ticket-resolve-date}}', $mainArray['ticket']['ticket_resolve_date'].' '.$mainArray['ticket']['ticket_resolve_time']);
  $tTemplate->bind('{{ticket-parent-id}}', $mainArray['ticket']['ticket_parent_id']);
  $tTemplate->bind('{{ticket-external-id}}', $mainArray['ticket']['ticket_external_id']);
  $tTemplate->bind('{{ticket-old-status}}', $mainArray['ticket']['ticket_status_id']);
  $tTemplate->bind('{{ticket-attachments}}', $attachments);
  $tTemplate->bind('{{ticket-events}}', $events);
  $tTemplate->bind('{{ticket-subscibers}}', $subscribers);
  $tTemplate->bind('{{ticket-time}}', $time);
  $tTemplate->bind('{{ticket-comments}}', $comments);
  $tTemplate->bind('{{ticket-comment-count}}', count($mainArray['comments']));
  $tTemplate->bind('{{queue-list}}', $ticket_queues);
  $tTemplate->bind('{{product-list}}', $ticket_products);
  $tTemplate->bind('{{priority-list}}', $ticket_priorities);
  $tTemplate->bind('{{env-list}}', $ticket_envs);
  $tTemplate->bind('{{impact-list}}', $ticket_impacts);
  $tTemplate->bind('{{subscriber-list}}', $ticket_subscribers);
  $tTemplate->bind('{{status-list}}', $ticket_statuses);
  $tTemplate->bind('{{ticket-queue-users}}', $ticket_queue_users);
  $tTemplate->bind('{{ticket-toolbar}}', $ticket_toolbar);
  $tTemplate->bind('{{ticket-reopen}}', $ticket_reopen_toolbar);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_TICKET_PREVIEW);
  $mTemplate->bind('{{page-content}}', $tTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
