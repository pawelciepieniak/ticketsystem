<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$_SESSION['user_id']);
  $User = $InstanceCollection->getUserList()[0];

  $id = null;
  $queryStringWhereCondition = '';

  if ($User->getQueueID() == 1)
  {
    $id = $User->getID();
    $queryStringWhereCondition = 't.ticket_requestor_id = '.$id;
  }
  else
  {
    $id = $User->getQueueID();
    $queryStringWhereCondition = 't.ticket_queue_id = '.$id;
  }

  $queues = array();
  $keys = array('new' => 1, 'analyse' => 2, 'in_progress' => 3, 'pending' => 4);

  $DatabaseHandler = $Database->connect();

  foreach ($keys as $key => $value)
  {
    $html = null;

    $statement =  $DatabaseHandler->prepare('SELECT
                                                    t.ticket_headline,
                                                    t.ticket_owner_id,
                                                    tpr.priority_name,
                                                    ti.impact_name,
                                                    u.user_name,
                                                    u.user_lastname,
                                                    t.ticket_id,
                                                    t.ticket_sla,
                                                    CASE
                                                      WHEN t.ticket_sla = "GOLD" THEN 3
                                                      WHEN t.ticket_sla = "SILVER" THEN 2
                                                      WHEN t.ticket_sla = "STANDARD" THEN 1
                                                    END AS sla_sort,
                                                    te.env_name,
                                                    tp.product_name,
                                                    t.ticket_create_date,
                                                    t.ticket_create_time
                                                FROM
                                                    ticket_db.tickets t
                                                LEFT JOIN ticket_db.users u ON
                                                    u.user_id = t.ticket_owner_id
                                                JOIN ticket_db.ticket_envs te ON
                                                    te.env_id = t.ticket_env_id
                                                JOIN ticket_db.ticket_products tp ON
                                                    tp.product_id = t.ticket_product_id
                                                JOIN ticket_db.ticket_priorities tpr ON
                                                    tpr.priority_id = t.ticket_priority_id
                                                JOIN ticket_db.ticket_impacts ti ON
                                                    ti.impact_id = t.ticket_impact_id
                                                WHERE 1 = 1
                                                AND '.$queryStringWhereCondition.'
                                                AND t.ticket_status_id = :ticket_status
                                                ORDER BY
                                                    sla_sort DESC,
                                                    t.ticket_env_id DESC,
                                                    t.ticket_create_date DESC,
                                                    t.ticket_create_time DESC');

    $statement->bindValue(':ticket_status', $value, PDO::PARAM_INT);
    $statement->execute();

    $queues[$key.'_count'] = $statement->rowCount();

    if ($statement->rowCount() > 0)
    {
      while ($data = $statement->fetch(PDO::FETCH_ASSOC))
      {
        $html .= '<a href="ticket.php?id='.$data['ticket_id'].'" class="ticket ticket-'.strtolower($data['ticket_sla']).'">
          <span class="ticket-headline" title="Tytuł zgłoszenia">['.$data['ticket_id'].'] '.$data['ticket_headline'].'</span>
          '.(empty($data['ticket_owner_id']) ? '<span class="ticket-owner ticket-unasigned" title="Osoba przypisana do realizacji zgłoszenia"><i class="fas fa-exclamation-triangle"></i>Nieprzypisany</span>' : '<span class="ticket-owner" title="Osoba przypisana do realizacji zgłoszenia"></i>'.$data['user_name'].' '.$data['user_lastname'].'</span>').'
          <table>
            <tr>
              <td title="Data stworzenia zgłoszenia"><i class="far fa-calendar-alt"></i>'.$data['ticket_create_date'].'</td>
              <td title="Data stworzenia zgłoszenia"><i class="far fa-clock"></i>'.$data['ticket_create_time'].'</td>
            </tr>
            <tr>
              <td title="Środowisko, którego dotyczy zgłoszenie"><i class="fas fa-angle-right"></i>'.$data['env_name'].'</td>
              <td title="Produkt, którego dotyczy zgłoszenie"><i class="fas fa-angle-right"></i>'.$data['product_name'].'</td>
            </tr>
            <tr>
              <td title="Priorytet"><i class="fas fa-angle-right"></i>'.$data['priority_name'].'</td>
              <td title="Impakt"><i class="fas fa-angle-right"></i>'.$data['impact_name'].'</td>
            </tr>
          </table>
        </a>';
      }
    }
    $queues[$key] = $html;
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $qTemplate = new TemplateBuilder();
  $qTemplate->prepare('src/templates/queue.html');
  $qTemplate->bind('{{tickets_new}}', $queues['new']);
  $qTemplate->bind('{{tickets_analyse}}', $queues['analyse']);
  $qTemplate->bind('{{tickets_inprogress}}', $queues['in_progress']);
  $qTemplate->bind('{{tickets_pending}}', $queues['pending']);
  $qTemplate->bind('{{tickets_new_count}}', $queues['new_count']);
  $qTemplate->bind('{{tickets_analyse_count}}', $queues['analyse_count']);
  $qTemplate->bind('{{tickets_inprogress_count}}', $queues['in_progress_count']);
  $qTemplate->bind('{{tickets_pending_count}}', $queues['pending_count']);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_DASHBOARD);
  $mTemplate->bind('{{page-content}}', $qTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
