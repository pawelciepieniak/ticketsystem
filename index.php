<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $DatabaseHandler = $Database->connect();
  $statement =  $DatabaseHandler->prepare('SELECT
                                              t.ticket_id,
                                              t.ticket_headline,
                                              t.ticket_sla,
                                              q.queue_name,
                                              l.event_type,
                                              l.event_date,
                                              u.user_name,
                                              u.user_lastname
                                          FROM
                                              ticket_db.app_event_log l
                                          JOIN ticket_db.tickets t ON
                                              t.ticket_id = l.event_ticket_id
                                          JOIN ticket_db.ticket_queues q ON
                                              q.queue_id = t.ticket_queue_id
                                          LEFT JOIN ticket_db.users u ON
                                              l.event_trigger_id = u.user_id
                                          WHERE 1 = 1
                                            AND t.ticket_owner_id = :user_id
                                             OR t.ticket_requestor_id = :user_id
                                       ORDER BY l.event_id DESC,
                                                l.event_date DESC');

  $statement->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
  $statement->execute();

  $htmlElement = null;

  if ($statement->rowCount() > 0)
  {
    while ($data = $statement->fetch(PDO::FETCH_ASSOC))
    {
      $htmlElement .=
      '<tr>
        <td><a href="ticket.php?id='.$data['ticket_id'].'"><i class="fas fa-link"></i> '.$data['ticket_id'].'</a></td>
        <td>'.$data['ticket_headline'].'</td>
        <td><span class="SLA SLA-'.ucfirst($data['ticket_sla']).'">'.$data['ticket_sla'].'</span></td>
        <td>'.$data['queue_name'].'</td>
        <td>'.$data['event_type'].'</td>
        <td>'.$data['event_date'].'</td>
        <td>'.$data['user_name'].' '.$data['user_lastname'].'</td>
      </tr>';
    }
  }
  else
  {
    $htmlElement .= '<tr><td colspan="7">Brak danych do wyświetlenia</td></tr>';
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $lTemplate = new TemplateBuilder();
  $lTemplate->prepare('src/templates/index.html');
  $lTemplate->bind('{{user-event-list}}', $htmlElement);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_INDEX);
  $mTemplate->bind('{{page-content}}', $lTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
