#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    create_ticket_log;
DELIMITER
    $$
CREATE TRIGGER create_ticket_log AFTER INSERT ON
    ticket_db.tickets FOR EACH ROW
BEGIN
    SET @ticket_id = NEW.ticket_id ;
    SET @trigger_id = NEW.ticket_requestor_id;
    SET @type = 'Utworzenie zgłoszenia';
    SET @date = NOW();

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date)
    VALUES( @ticket_id, @trigger_id, @type, @date);
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    upadte_ticket_log;
DELIMITER
    $$
CREATE TRIGGER upadte_ticket_log AFTER UPDATE ON
    ticket_db.tickets FOR EACH ROW
BEGIN
  # QUEUE
  IF OLD.ticket_queue_id <> NEW.ticket_queue_id
  THEN
    SELECT queue_name INTO @new_queue_name FROM ticket_queues WHERE queue_id = NEW.ticket_queue_id;
    SELECT queue_name INTO @old_queue_name FROM ticket_queues WHERE queue_id = OLD.ticket_queue_id;

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date, event_details)
    VALUES(NEW.ticket_id, 0, 'Zmiana kolejki', NOW(), CONCAT(@old_queue_name,' -> ', @new_queue_name) );
  END IF;

  # STATUS
  IF OLD.ticket_status_id  <> NEW.ticket_status_id
  THEN
    SELECT ticket_status_name INTO @new_status_name FROM ticket_statuses WHERE ticket_status_id = NEW.ticket_status_id;
    SELECT ticket_status_name INTO @old_status_name FROM ticket_statuses WHERE ticket_status_id = OLD.ticket_status_id;

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date, event_details)
    VALUES(NEW.ticket_id, 0, 'Zmiana statusu', NOW(), CONCAT(@old_status_name,' -> ', @new_status_name) );
  END IF;

  # OWNER
  IF OLD.ticket_owner_id <> NEW.ticket_owner_id
  THEN
    SELECT CONCAT(user_name,' ', user_lastname) INTO @new_owner FROM users WHERE user_id = NEW.ticket_owner_id;
    SELECT CONCAT(user_name,' ', user_lastname) INTO @old_owner FROM users WHERE user_id = OLD.ticket_owner_id;

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date, event_details)
    VALUES(NEW.ticket_id, 0, 'Zmiana właściciela', NOW(), CONCAT(@old_owner,' -> ', @new_owner) );
  END IF;
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    create_reply_log;
DELIMITER
    $$
CREATE TRIGGER create_reply_log AFTER INSERT ON
    ticket_db.ticket_comments FOR EACH ROW
BEGIN
    SET @ticket_id = NEW.comment_ticket_id;
    SET @trigger_id = NEW.comment_author_id;
    SET @type = 'Dodanie komentarza';
    SET @date = NOW();

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date)
    VALUES( @ticket_id, @trigger_id, @type, @date);
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    create_attachments_log;
DELIMITER
    $$
CREATE TRIGGER create_attachments_log AFTER INSERT ON
    ticket_db.ticket_attachments FOR EACH ROW
BEGIN
    SET @ticket_id = NEW.attachment_ticket_id;
    SET @trigger_id = NEW.attachment_owner_id;
    SET @type = 'Dodanie załącznika';
    SET @date = NOW();

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date)
    VALUES( @ticket_id, @trigger_id, @type, @date);
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    create_subscriber_log;
DELIMITER
    $$
CREATE TRIGGER create_subscriber_log AFTER INSERT ON
    ticket_db.ticket_subscribers FOR EACH ROW
BEGIN
    SET @ticket_id = NEW.ticket_id;
    SET @trigger_id = NEW.inviter_id;
    SET @type = 'Dodanie obserwującego';
    SET @date = NOW();

	SELECT CONCAT(user_name,' ', user_lastname) INTO @subscriber FROM users WHERE user_id = NEW.user_id;

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date, event_details)
    VALUES( @ticket_id, @trigger_id, @type, @date, @subscriber);
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS
    delete_subscriber_log;
DELIMITER
    $$
CREATE TRIGGER delete_subscriber_log BEFORE DELETE ON
    ticket_db.ticket_subscribers FOR EACH ROW
BEGIN
    SET @ticket_id = OLD.ticket_id;
    SET @trigger_id = 0;
    SET @type = 'Usunięcie obserwującego';
    SET @date = NOW();

	SELECT CONCAT(user_name,' ', user_lastname) INTO @subscriber FROM users WHERE user_id = OLD.user_id;

    INSERT INTO ticket_db.app_event_log(event_ticket_id, event_trigger_id, event_type, event_date, event_details)
    VALUES( @ticket_id, @trigger_id, @type, @date, @subscriber);
END $$
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
