-- Application configuration
CREATE TABLE IF NOT EXISTS app_config (
  email_sender_service TINYINT UNSIGNED NOT NULL,
  system_active TINYINT UNSIGNED NOT NULL
) ENGINE = MyISAM;

-- Tickets
CREATE TABLE IF NOT EXISTS tickets (
  ticket_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  ticket_sla VARCHAR(8) NOT NULL,
  ticket_status_id TINYINT UNSIGNED NOT NULL, -- Forgein Key | ticket_statuses.ticket_status_id
  ticket_queue_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | ticket_queues.queue_id
  ticket_owner_id BIGINT UNSIGNED, -- Forgein Key | users.user_id
  ticket_requestor_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | user_id
  ticket_product_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | ticket_products.product_id
  ticket_env_id TINYINT UNSIGNED NOT NULL, -- Forgein Key | ticket_envs.env_id
  ticket_priority_id TINYINT UNSIGNED NOT NULL, -- Forgein Key | ticket_priorities.priority_id
  ticket_impact_id TINYINT UNSIGNED NOT NULL, -- Forgein Key | ticket_impacts.impact_id
  ticket_parent_id BIGINT UNSIGNED,
  ticket_external_id BIGINT UNSIGNED,
  ticket_create_date DATE NOT NULL,
  ticket_create_time TIME NOT NULL,
  ticket_resolve_date DATE,
  ticket_resolve_time TIME,
  ticket_headline VARCHAR(50) NOT NULL,
  ticket_description TEXT
) ENGINE = MyISAM;

-- List of ticket comments
CREATE TABLE IF NOT EXISTS ticket_comments(
  comment_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  comment_ticket_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | tickets.ticket_id
  comment_description TEXT,
  comment_author_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | users.user_id
  comment_create_date DATETIME NOT NULL
) ENGINE = MyISAM;

-- List of ticket attachment
CREATE TABLE IF NOT EXISTS ticket_attachments(
  attachment_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  attachment_ticket_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | tickets.ticket_id
  attachment_comment_id BIGINT UNSIGNED NOT NULL, -- Forgein Key | ticket_comments.comment_id
  attachment_upload_date DATETIME NOT NULL,
  attachment_mime_type VARCHAR(20) NOT NULL,
  attachment_source_path VARCHAR(100) NOT NULL,
  attachment_owner_id BIGINT UNSIGNED NOT NULL -- Forgein Key | users.user_id
) ENGINE = MyISAM;

-- List of ticket statuses
CREATE TABLE IF NOT EXISTS ticket_statuses (
  ticket_status_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  ticket_status_name VARCHAR(50)
) ENGINE = MyISAM;

-- List of ticket queues
CREATE TABLE IF NOT EXISTS ticket_queues (
  queue_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  queue_name VARCHAR(50)
) ENGINE = MyISAM;

-- Queues processflow
CREATE TABLE IF NOT EXISTS queues_flow (
  queue_id BIGINT UNSIGNED NOT NULL,
  target_queue_id BIGINT UNSIGNED NOT NULL
);

-- List of ticket system/products
CREATE TABLE IF NOT EXISTS ticket_products (
  product_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  product_name VARCHAR(50)
) ENGINE = MyISAM;

-- List of ticket enviroments
CREATE TABLE IF NOT EXISTS ticket_envs (
  env_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  env_name VARCHAR(50)
) ENGINE = MyISAM;

-- List of ticket priorities
CREATE TABLE IF NOT EXISTS ticket_priorities (
  priority_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  priority_name VARCHAR(50)
) ENGINE = MyISAM;

-- List of ticket impacts
CREATE TABLE IF NOT EXISTS ticket_impacts (
  impact_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  impact_name VARCHAR(50)
) ENGINE = MyISAM;

-- List of ticket subscribers
CREATE TABLE IF NOT EXISTS ticket_subscribers (
  ticket_id BIGINT UNSIGNED NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL,
  inviter_id BIGINT UNSIGNED NOT NULL,
  event_date DATETIME
) ENGINE = MyISAM;

-- Ticket work time
CREATE TABLE IF NOT EXISTS ticket_work_time (
  ticket_id BIGINT UNSIGNED NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL,
  time_spend INT NOT NULL,
  event_date DATETIME
) ENGINE = MyISAM;

-- Users
CREATE TABLE IF NOT EXISTS users (
  user_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  user_name VARCHAR(50),
  user_lastname VARCHAR(50),
  user_email VARCHAR(50),
  user_password VARCHAR(255),
  user_phone_number VARCHAR(12),
  user_account_status TINYINT NOT NULL,
  user_queue_id BIGINT UNSIGNED NOT NULL, -- Forgein key | ticket_queues.queue_id
  user_reg_date DATETIME NOT NULL,
  user_last_online_date DATETIME
) ENGINE = MyISAM;

-- Table contains changes / events on tickets
CREATE TABLE IF NOT EXISTS app_event_log (
  event_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  event_ticket_id BIGINT NOT NULL,
  event_trigger_id BIGINT NOT NULL,
  event_type VARCHAR(100),
  event_date DATETIME NOT NULL,
  event_details TEXT
) ENGINE = MyISAM;

-- Admins
CREATE TABLE IF NOT EXISTS admins (
  admin_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  admin_login VARCHAR(50),
  admin_email VARCHAR(50),
  admin_password VARCHAR(255)
) ENGINE = MyISAM;

-- Create admin root/parole1
INSERT INTO admins VALUES(
  NULL,
  'root',
  'root@domain.com',
  '$2y$10$25RA9YiL0EIv/Lben9NIruhTtyxGMsNt5yVk36onOfmZ/l.vZomw2'
);

-- Create requestor queue
INSERT INTO ticket_queues VALUES(
  NULL,
  'Zgłaszający'
);
