<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == true) $App->redirect('index.php');
  #-----------------------------------------------------------------------------
  # Site logic
  #-----------------------------------------------------------------------------
  if (isset($_POST['login-submit']))
  {
    $DatabaseHandler = $Database->connect();

    $login = trim($_POST['user-login']);;
    $password = trim($_POST['user-password']);

    # Check empty fields
    if (empty($login) || empty($password))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie pola</div>');
    }
    else
    {
      # Check is login exists in database
      $statement = $DatabaseHandler->prepare('SELECT user_id,
                                                     user_email,
                                                     user_password
                                                FROM ticket_db.users
                                               WHERE user_email = :user_email');

      $statement->bindValue(':user_email', $login, PDO::PARAM_STR);
      $statement->execute();

      if ($statement->rowCount() > 0)
      {
        $data = $statement->fetch(PDO::FETCH_ASSOC);

        if (password_verify($password, $data['user_password']))
        {
          $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$data['user_id']);
          $User = $InstanceCollection->getUserList()[0];

          if ($User->getAccountStatus() == 0)
          {
            $MessageCollection->add('<div class="message message-error">Twoje konto zostało zablokowane przez administratora</div>');
          }
          else
          {
            $User->updateLastOnline();
            $App->createSession($User->getID());
            $App->redirect('index.php');
          }
        }
        else
        {
          $MessageCollection->add('<div class="message message-error">Niepoprawny login lub hasło</div>');
        }
      }
      else
      {
        $MessageCollection->add('<div class="message message-error">Niepoprawny login lub hasło</div>');
      }
    }
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $lTemplate = new TemplateBuilder();
  $lTemplate->prepare('src/templates/form-login.html');

  if ($MessageCollection->size() > 0)
    $lTemplate->bind('{{error}}', $MessageCollection->getValueByIndex(0));
  else
    $lTemplate->bind('{{error}}', null);

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_LOGIN);
  $mTemplate->bind('{{page-content}}', $lTemplate->render());

  echo $mTemplate->render();
?>
