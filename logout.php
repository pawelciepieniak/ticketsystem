<?php
  require_once 'src/includes/config.php';

  # Check if user is login
  if ($App->checkSession() == true)
  {
    $App->destroySession();
  }

  $App->redirect('login.php');
?>
