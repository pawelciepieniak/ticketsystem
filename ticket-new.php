<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$_SESSION['user_id']);
  $User = $InstanceCollection->getUserList()[0];

  $Ticket = new Ticket();

  $ticket_queues = null;
  $ticket_products = null;
  $ticket_priorities = null;
  $ticket_envs = null;
  $ticket_impacts = null;
  $ticket_subscribers = null;

  foreach ($Ticket->getQueueList($User->getQueueID()) as $row) $ticket_queues .= '<option value="'.$row['queue_id'].'">'.$row['queue_name'].'</option>';
  foreach ($Ticket->getProductList() as $row) $ticket_products .= '<option value="'.$row['product_id'].'">'.$row['product_name'].'</option>';
  foreach ($Ticket->getPriorityList() as $row) $ticket_priorities .= '<option value="'.$row['priority_id'].'">'.$row['priority_name'].'</option>';
  foreach ($Ticket->getEnvList() as $row) $ticket_envs .= '<option value="'.$row['env_id'].'">'.$row['env_name'].'</option>';
  foreach ($Ticket->getImpactList() as $row) $ticket_impacts .= '<option value="'.$row['impact_id'].'">'.$row['impact_name'].'</option>';
  foreach ($Ticket->getSubscriberList($User->getQueueID(), $User->getID()) as $row) $ticket_subscribers .= '<option value="'.$row['user_id'].'">'.$row['user_name'].' '.$row['user_lastname'].' ('.$row['user_email'].')</option>';
  #-----------------------------------------------------------------------------
  # Site logic
  #-----------------------------------------------------------------------------
  $tTemplate = new TemplateBuilder();

  if (isset($_POST['ticket_new_submit']))
  {
    # Ticket variables
              $ticket_id = null;
             $ticket_sla = null;
       $ticket_status_id = 1;
        $ticket_queue_id = intval($_POST['ticket_queue']);
        $ticket_owner_id = null;
    $ticket_requestor_id = $User->getID();
      $ticket_product_id = intval($_POST['ticket_product']);
          $ticket_env_id = intval($_POST['ticket_env']);
     $ticket_priority_id = intval($_POST['ticket_priority']);
       $ticket_impact_id = intval($_POST['ticket_impact']);
       $ticket_parent_id = intval($_POST['ticket_parent_id']);
     $ticket_external_id = intval($_POST['ticket_external_id']);
     $ticket_create_date = date('Y-m-d');
     $ticket_create_time = date('H:i:s');
    $ticket_resolve_date = null;
    $ticket_resolve_time = null;
        $ticket_headline = trim($_POST['ticket_headline']);
     $ticket_description = trim($_POST['ticket_description']);

    # Ticket subscrubers
            $subscribers = @$_POST['subscribers'];
   $subscriber_ticket_id = null;
     $subscriber_user_id = null;
  $subscriber_inviter_id = $User->getID();
  $subscriber_event_date = date('Y-m-d H:i:s');

    # Attachments variables
            $attachments = $_FILES['ticket_attachments'];
          $attachment_id = null;
   $attachment_ticket_id = null;
  $attachment_comment_id = null;
 $attachment_upload_date = null;
   $attachment_mime_type = null;
 $attachment_source_path = null;
    $attachment_owner_id = null;

    # Check empty fields
    if (empty($ticket_headline) || empty($ticket_queue_id) || empty($ticket_product_id) || empty($ticket_priority_id) || empty($ticket_env_id) || empty($ticket_impact_id) || empty($ticket_description))
    {
      $MessageCollection->add('<div class="message message-error">Wypełnij wszystkie wymagane pola</div>');
    }
    else
    {
      # Count ticket SLA
      $ticket_sla = $Ticket->getSLA($ticket_env_id, $ticket_impact_id, $ticket_priority_id);

      $DatabaseHandler = $Database->connect();

      # Start transaction
      try
      {
        $DatabaseHandler->beginTransaction();

        # Insert ticket
        $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.tickets VALUES(:ticket_id, :ticket_sla, :ticket_status_id, :ticket_queue_id, :ticket_owner_id, :ticket_requestor_id, :ticket_product_id, :ticket_env_id, :ticket_priority_id, :ticket_impact_id, :ticket_parent_id, :ticket_external_id, :ticket_create_date, :ticket_create_time, :ticket_resolve_date, :ticket_resolve_time, :ticket_headline, :ticket_description)');

        $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_sla', $ticket_sla, PDO::PARAM_STR);
        $statement->bindValue(':ticket_status_id', $ticket_status_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_queue_id', $ticket_queue_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_owner_id', $ticket_owner_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_requestor_id', $ticket_requestor_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_product_id', $ticket_product_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_env_id', $ticket_env_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_priority_id', $ticket_priority_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_impact_id', $ticket_impact_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_parent_id', $ticket_parent_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_external_id', $ticket_external_id, PDO::PARAM_INT);
        $statement->bindValue(':ticket_create_date', $ticket_create_date, PDO::PARAM_STR);
        $statement->bindValue(':ticket_create_time', $ticket_create_time, PDO::PARAM_STR);
        $statement->bindValue(':ticket_resolve_date', $ticket_resolve_date, PDO::PARAM_STR);
        $statement->bindValue(':ticket_resolve_time', $ticket_resolve_time, PDO::PARAM_STR);
        $statement->bindValue(':ticket_headline', $ticket_headline, PDO::PARAM_STR);
        $statement->bindValue(':ticket_description', $ticket_description, PDO::PARAM_STR);
        $statement->execute();

        $ticket_id = $DatabaseHandler->lastInsertId();

        # Insert subscribers
        if (count($subscribers) > 0)
        {
          $subscriber_ticket_id = $ticket_id;

          $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.ticket_subscribers VALUES(:ticket_id, :user_id, :inviter_id, :event_date)');

          foreach ($subscribers as $subscriber)
          {
            $subscriber_user_id = $subscriber;

            $statement->bindValue(':ticket_id', $subscriber_ticket_id, PDO::PARAM_INT);
            $statement->bindValue(':user_id', $subscriber_user_id, PDO::PARAM_INT);
            $statement->bindValue(':inviter_id', $subscriber_inviter_id, PDO::PARAM_INT);
            $statement->bindValue(':event_date', $subscriber_event_date, PDO::PARAM_STR);
            $statement->execute();
          }
        }

        # Upload and save attachments
        $count_of_attachments = count(array_filter($attachments['name']));

        //$App->debug($_FILES);
        //$App->debug($count_of_attachments);

        if (count($count_of_attachments) > 0)
        {

          $target_dir = ATTACHMENT_TARGET_DIR;
          mkdir($target_dir.$ticket_id, 7777, true);

          for ($i = 0; $i < $count_of_attachments; $i++)
          {
            $uploadOk = 1;
            $target_file = $target_dir.$ticket_id.'/'.basename($attachments['name'][$i]);

            $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            if (file_exists($target_file))
            {
                $MessageCollection->add('Plik '.basename($attachments['name'][$i]).' już istnieje, prosimy zmień nazwę pliku.');
                $uploadOk = 0;
            }

            if ($attachments['size'][$i] > $App->file_upload_max_size())
            {
                $MessageCollection->add('Plik: '.basename($attachments['name'][$i]).' zbyt duży ('.$attachments['size'].'). Maksymalny rozmiar pliku wynosi '.$App->file_upload_max_size());
                $uploadOk = 0;
            }

            if ($uploadOk == 1)
            {
              if (!move_uploaded_file($attachments['tmp_name'][$i], $target_file))
              {
                $MessageCollection->add('Wystąpił błąd podczas wgrywania pliku: '.basename($attachments['name'][$i]));
              }
              else
              {
                $attachment_id = null;
                $attachment_ticket_id = $ticket_id;
                $attachment_comment_id = 0;
                $attachment_upload_date = date('Y-m-d H:i:s');
                $attachment_mime_type = $attachments['type'][$i];
                $attachment_source_path = $target_file;
                $attachment_owner_id = $User->getID();

                $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.ticket_attachments VALUES(:id, :ticket_id, :comment_id, :upload_date, :mime_type, :source_path, :owner_id)');
                $statement->bindValue(':id', $attachment_id, PDO::PARAM_STR);
                $statement->bindValue(':ticket_id', $attachment_ticket_id, PDO::PARAM_INT);
                $statement->bindValue(':comment_id', $attachment_comment_id, PDO::PARAM_STR);
                $statement->bindValue(':upload_date', $attachment_upload_date, PDO::PARAM_STR);
                $statement->bindValue(':mime_type', $attachment_mime_type, PDO::PARAM_STR);
                $statement->bindValue(':source_path', $attachment_source_path, PDO::PARAM_STR);
                $statement->bindValue(':owner_id', $attachment_owner_id, PDO::PARAM_INT);
                $statement->execute();
              }
            }
          }
        }

        # Make commit
        $DatabaseHandler->commit();

        # Send email
        if ($app_config['email_sender_service'] == 1)
        {
          foreach ($Ticket->getQueueUserList($ticket_queue_id) as $row)
          {
            $EmailSender->setRecipient($row['user_email']);
            $EmailSender->setSubject('Ticket System | Nowe zgłoszenie pojawiło się w Twojej kolejce');
            $EmailSender->setHeaders();
            $EmailSender->prepare('src/templates/emails/new-ticket.html');
            $EmailSender->bind('{{email_subject}}',$EmailSender->getSubject());
            $EmailSender->bind('{{ticket-id}}', $ticket_id);
            $EmailSender->bind('{{ticket-sla}}', $ticket_sla);
            $EmailSender->bind('{{ticket-headline}}', $ticket_headline);
            $EmailSender->bind('{{ticket-description}}', $ticket_description);
            $EmailSender->setMessage($EmailSender->render());
            $EmailSender->send();
          }
        }
      }
      catch (PDOException $e)
      {
        $DatabaseHandler->rollback();
        //$App->debug($e);
      }
    }
    $tTemplate->prepare('src/templates/ticket-new-submit.html');

    $output = null;

    if ($MessageCollection->size() > 0)
    {
      for ($i = 0; $i < $MessageCollection->size(); $i++) $output .= '<div class="message message-error">'.$MessageCollection->getValueByIndex($i).'</div>';
    }

    $output .= '<div class="message message-success">Twoje zgłoszenie zostało pomyślnie dodane. Kilknij <strong><a href="ticket.php?id='.$ticket_id.'">tutaj</a></strong> aby je zobaczyć</div>';

    $tTemplate->bind('{{content}}', $output);

    //$App->redirect('ticket.php?id='.$ticket_id);
  }
  else
  {
    $tTemplate->prepare('src/templates/ticket-new.html');
    $tTemplate->bind('{{queue-list}}', $ticket_queues);
    $tTemplate->bind('{{product-list}}', $ticket_products);
    $tTemplate->bind('{{priority-list}}', $ticket_priorities);
    $tTemplate->bind('{{env-list}}', $ticket_envs);
    $tTemplate->bind('{{impact-list}}', $ticket_impacts);
    $tTemplate->bind('{{subscriber-list}}', $ticket_subscribers);
  }
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $mTemplate->bind('{{page-title}}', PAGE_TITLE_TICKET_NEW);
  $mTemplate->bind('{{page-content}}', $tTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
