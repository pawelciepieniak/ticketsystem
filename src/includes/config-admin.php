<?php session_start();

  # Require PHP classes
  require_once '../src/classes/InstanceCollection.class.php';
  require_once '../src/classes/MessageCollection.class.php';
  require_once '../src/classes/TemplateBuilder.class.php';
  require_once '../src/classes/EmailSender.class.php';
  require_once '../src/classes/Database.class.php';
  require_once '../src/classes/Product.class.php';
  require_once '../src/classes/Queue.class.php';
  require_once '../src/classes/User.class.php';
  require_once '../src/classes/App.class.php';

  # Define constants
  define('PAGE_TITLE_LOGIN', 'Panel administracyjny - logowanie');
  define('PAGE_TITLE_INDEX', 'Panel administracyjny - kokpit');
  define('PAGE_TITLE_USERS', 'Panel administracyjny - użytkownicy');
  define('PAGE_TITLE_QUEUES', 'Panel administracyjny - kolejki');
  define('PAGE_TITLE_PRODUCTS', 'Panel administracyjny - systemy/produkty');
  define('PAGE_TITLE_CONFIG', 'Panel administracyjny - ustawienia');
  define('PAGE_TITLE_QUEUE_FLOW', 'Panel administracyjny - procesflow');

  # Initialize instances of classes
                 $App = new App();
            $Database =  new Database();
         $EmailSender = new EmailSender();
   $MessageCollection = new MessageCollection();
  $InstanceCollection = new InstanceCollection();

  # Get application configuration
  $app_config = $InstanceCollection->getConfig();

  # Load main HTML template
  $MainTemplate = new TemplateBuilder();
  $MainTemplate->prepare('../src/templates/admin-main.html');
?>
