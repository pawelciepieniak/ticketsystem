<?php session_start();
  #-----------------------------------------------------------------------------
  # Require PHP classes
  #-----------------------------------------------------------------------------
  require_once 'src/classes/InstanceCollection.class.php';
  require_once 'src/classes/MessageCollection.class.php';
  require_once 'src/classes/TemplateBuilder.class.php';
  require_once 'src/classes/EmailSender.class.php';
  require_once 'src/classes/Database.class.php';
  require_once 'src/classes/Product.class.php';
  require_once 'src/classes/Ticket.class.php';
  require_once 'src/classes/Queue.class.php';
  require_once 'src/classes/User.class.php';
  require_once 'src/classes/App.class.php';
  #-----------------------------------------------------------------------------
  # Define constants
  #-----------------------------------------------------------------------------
  define('PAGE_TITLE_LOGIN', 'Panel logowania');
  define('PAGE_TITLE_INDEX', 'Strona główna');
  define('PAGE_TITLE_TICKET_NEW', 'Nowe zgłoszenie');
  define('PAGE_TITLE_DASHBOARD', 'Dashboard');
  define('PAGE_TITLE_USER_PROFILE', 'Profil użytkownika');
  define('PAGE_TITLE_TICKET_SEARCH', 'Szukaj zgłoszeń');
  define('PAGE_TITLE_TICKET_SELF', 'Twoje utworzone zgłoszenia');
  define('PAGE_TITLE_TICKET_PREVIEW', 'Podgląd zgłoszenia');

  define('ATTACHMENT_TARGET_DIR', 'attachments/');
  #-----------------------------------------------------------------------------
  # Initialize instances of classes
  #-----------------------------------------------------------------------------
                 $App = new App();
            $Database = new Database();
         $EmailSender = new EmailSender();
   $MessageCollection = new MessageCollection();
  $InstanceCollection = new InstanceCollection();
  #-----------------------------------------------------------------------------
  # Get application configuration
  #-----------------------------------------------------------------------------
  $app_config = $InstanceCollection->getConfig();

  if (intval($app_config['system_active']) == 0)
  {
    die('Application has been shouted down by administrator.');
  }

  $App->closeResolvedTickets($Database);
  #-----------------------------------------------------------------------------
  # Load main HTML template
  #-----------------------------------------------------------------------------
  $tPanel = new TemplateBuilder();
  $tPanel->prepare('src/templates/top-panel.html');

  $mTemplate = new TemplateBuilder();
  $mTemplate->prepare('src/templates/main.html');
?>
