$(document).ready(function(){
  //----------------------------------------------------------------------------
  // Login form
  //----------------------------------------------------------------------------
  $('.delete-user-button').click(function(){
    var user_id = $(this).attr('data-id');
    var name = $(this).parent().parent().children('td:nth-child(2)').text();
    var lastname = $(this).parent().parent().children('td:nth-child(3)').text();

    $('#form-ticket-time-pop-up input[name="user_id"]').val(user_id);
    $('#form-ticket-time-pop-up span b').text(name + ' ' + lastname);

    $('.pop-up-wrapper').css('display', 'flex');
  });

  $('.delete-product-button').click(function(){
    var product_id = $(this).attr('data-id');
    var name = $(this).parent().parent().children('td[name="product-name"]').text();

    $('#form-ticket-time-pop-up input[name="product_id"]').val(product_id);
    $('#form-ticket-time-pop-up span b').text(name);

    $('.pop-up-wrapper').css('display', 'flex');
  });

  $('.delete-queue-button').click(function(){
    var queue_id = $(this).attr('data-id');
    var name = $(this).parent().parent().children('td[name="queue-name"]').text();

    $('#form-ticket-time-pop-up input[name="queue_id"]').val(queue_id);
    $('#form-ticket-time-pop-up span b').text(name);

    $('.pop-up-wrapper').css('display', 'flex');
  });
  //----------------------------------------------------------------------------
  // Close pop up
  //----------------------------------------------------------------------------
  $('.cancel-pop-up').click(function(){
    $('.pop-up-wrapper').css('display', 'none');
  });
  //----------------------------------------------------------------------------
  // Show forms
  //----------------------------------------------------------------------------
  $('.edit-user-button').click(function(){
    var form_edit = $('#admin-form-edit-user');
    var form_create = $('#admin-form-new-user');

    form_create.css('display', 'none');
    form_edit.css('display', 'block');

    var id = $(this).parent().parent().children('td[name="user-id"]').text();
    var name = $(this).parent().parent().children('td[name="user-name"]').text();
    var lastname = $(this).parent().parent().children('td[name="user-lastname"]').text();
    var email = $(this).parent().parent().children('td[name="user-email"]').text();
    var phone_number = $(this).parent().parent().children('td[name="user-phone-number"]').text();
    var account_status = $(this).parent().parent().children('td[name="user-account-status"]').attr('data-id');
    var queue_id = $(this).parent().parent().children('td[name="user-queue-id"]').attr('data-id');

    form_edit.find('input[name="user_id"]').val(id);
    form_edit.find('input[name="user_name"]').val(name);
    form_edit.find('input[name="user_lastname"]').val(lastname);
    form_edit.find('input[name="user_email"]').val(email);
    form_edit.find('input[name="user_old_email"]').val(email);
    form_edit.find('input[name="user_phone_number"]').val(phone_number);
    form_edit.find('select[name="user_account_status"]').val(account_status);
    form_edit.find('select[name="user_queue"]').val(queue_id);
  });

  $('.edit-product-button').click(function(){
    var form_edit = $('#admin-form-edit-product');
    var form_create = $('#admin-form-new-product');

    form_create.css('display', 'none');
    form_edit.css('display', 'block');

    var id = $(this).parent().parent().children('td[name="product-id"]').text();
    var name = $(this).parent().parent().children('td[name="product-name"]').text();

    form_edit.find('input[name="product_id"]').val(id);
    form_edit.find('input[name="product_name"]').val(name);
  });

  $('.edit-queue-button').click(function(){
    var form_edit = $('#admin-form-edit-queue');
    var form_create = $('#admin-form-new-queue');

    form_create.css('display', 'none');
    form_edit.css('display', 'block');

    var id = $(this).parent().parent().children('td[name="queue-id"]').text();
    var name = $(this).parent().parent().children('td[name="queue-name"]').text();

    form_edit.find('input[name="queue_id"]').val(id);
    form_edit.find('input[name="queue_name"]').val(name);
  });
  //----------------------------------------------------------------------------
  // Cancel edit forms
  //----------------------------------------------------------------------------
  $('.edit-user-cancel').click(function(){
    var form_edit = $('#admin-form-edit-user');
    var form_create = $('#admin-form-new-user');

    form_create.css('display', 'block');
    form_edit.css('display', 'none');
  });

  $('.edit-product-cancel').click(function(){
    var form_edit = $('#admin-form-edit-product');
    var form_create = $('#admin-form-new-product');

    form_create.css('display', 'block');
    form_edit.css('display', 'none');
  });

  $('.edit-queue-cancel').click(function(){
    var form_edit = $('#admin-form-edit-queue');
    var form_create = $('#admin-form-new-queue');

    form_create.css('display', 'block');
    form_edit.css('display', 'none');
  });
  //----------------------------------------------------------------------------
  // Fix view for login page
  //----------------------------------------------------------------------------
  var location_pathname = window.location.pathname;
  var location_split = location_pathname.split('/');

  if (location_split[location_split.length - 1] == 'login.php'){
    $('#left-admin-panel').css('display', 'none');
    $('#right-admin-panel').css('width', '100%');
    $('#right-admin-panel').css('padding', '0 0');
    $('#right-admin-panel').css('background-color', 'var(--bodyBackgroundColor)');
    $('#right-admin-panel h1').css('display', 'none');
    $('#form-login-wrapper h1').css('display', 'block');
  }
  //----------------------------------------------------------------------------
});
