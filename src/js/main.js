$(document).ready(function(){
  //----------------------------------------------------------------------------
  // Scriplib
  //----------------------------------------------------------------------------
  function formatBytes(bytes, decimals = 2) {
      if (bytes === 0) return '0 Bytes';

      const k = 1024;
      const dm = decimals < 0 ? 0 : decimals;
      const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

      const i = Math.floor(Math.log(bytes) / Math.log(k));

      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  var scrollToElement = function(elementId){
    $('html, body').animate({scrollTop: $("#" + elementId).offset().top}, 2000);
  }
  //----------------------------------------------------------------------------
  // Ticket new - attachments
  //----------------------------------------------------------------------------
  $('#form-new-ticket input[type="file"]').change(function(e){
    var container = $('#form-ticket-attachments .form-ticket-cell-4 tbody');
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var date;
    var attachments = e.target.files;

    //console.log(attachments);

    container.html('');

    for (i = 0; i < attachments.length; i ++){
      date = new Date(attachments[i].lastModified);;
      container.append('<tr><td>' + (i + 1) + '</td><td>' + attachments[i].name + '</td><td>' + formatBytes(attachments[i].size) + '</td><td>' + attachments[i].type + '</td><td>' + date.toLocaleString() + '</td></tr>');
    }
  });
  //----------------------------------------------------------------------------
  // Ticket new - subscribers
  //----------------------------------------------------------------------------
  var history = [];
  $('#subscibers-toolbar button').click(function(e){
    var value = $(this).parent().children('select');
    var targetText = $('#subscribers-table tbody');
    var targetInput = $('#subscibers-container');

    console.log(history);

    if (history.includes(value.val()) == false){
      targetText.append('<tr><td class="text-center"><button type="button" class="remove-subscriber" data-id="'+ value.val() + '">Usuń</button></td><td>' + value.children('option:selected').text() + '</td></tr>');
      targetInput.append('<input type="hidden" data-id="'+ value.val() + '" name="subscribers[]" value="'+ value.val() + '"/>');
      history.push(value.val());
    } else {
      alert("Subskrybent " + value.children('option:selected').text() + " już został dodany");
    }
    e.preventDefault();
  });

  $('body').on('click', '.remove-subscriber', function(){
    var id = $(this).attr('data-id');

    for( var i = 0; i < history.length; i++){
       if ( history[i] == $(this).attr('data-id')) {
         history.splice(i, 1);
         $(this).parent().parent().remove();
         $('#subscibers-container input[data-id="'+id+'"]').remove();
       }
    }
  });
  //----------------------------------------------------------------------------
  // Ticket preview - tabs
  //----------------------------------------------------------------------------
  $('#ticket-section-tabs a').click(function(e){
    var tabButton = $(this);
    var tabContainer = tabButton.attr('tab-id');

    tabButton.parent().children('a').removeClass('active-tab-button');
    tabButton.addClass('active-tab-button');

    $("#ticket-attachments").removeClass('ticket-section-active');
    $("#ticket-history").removeClass('ticket-section-active');
    $("#ticket-subscribers").removeClass('ticket-section-active');
    $("#ticket-tags").removeClass('ticket-section-active');

    $('#' + tabContainer).addClass('ticket-section-active');

    e.preventDefault();
  });
  //----------------------------------------------------------------------------
  // Ticket preview - toolbar
  //----------------------------------------------------------------------------
  var isFormDisabled = 1;

  $('#ticket-btn-edit').click(function(e){
    if (isFormDisabled == 1){
      $('#ticket-summary select, #ticket-summary input').attr('disabled', false);
      $('#ticket-summary select, #ticket-summary input').css('border-color', 'var(--inputBorderColor)');
      $('#ticket-summary button').fadeIn('fast');
      $(this).html('<i class="fas fa-window-close"></i>Anuluj edytowanie');
      isFormDisabled = 0;
    } else {
      $('#ticket-summary select, #ticket-summary input').attr('disabled', true);
      $('#ticket-summary select, #ticket-summary input').css('border-color', 'transparent');
      $('#ticket-summary button').fadeOut('fast');
      $(this).html('<i class="fas fa-pencil-alt"></i>Edytuj ogólne');
      isFormDisabled = 1;
    }

    e.preventDefault();
  });

  $('#ticket-btn-comment').click(function(e){
    scrollToElement('ticket-reply');
    e.preventDefault();
  });

  $('#ticket-btn-comment').click(function(e){
    scrollToElement('ticket-reply');
    e.preventDefault();
  });

  $('#ticket-btn-time').click(function(e){
    $('.pop-up-wrapper').css('display', 'flex');
  });

  $('#ticket-subscribers-delete input').click(function(){
    var numberOfChecked = $('input:checkbox:checked').length;

    if (numberOfChecked > 0) {
      $('button[name="delete-subscriber-submit"]').fadeIn(0);
    } else {
      $('button[name="delete-subscriber-submit"]').fadeOut(0);
    }
  });
  //----------------------------------------------------------------------------
  // Close pop up
  //----------------------------------------------------------------------------
  $('.cancel-pop-up').click(function(){
    $('.pop-up-wrapper').css('display', 'none');
  });
/*
//----------------------------------------------------------------------------
// Scriplib
//----------------------------------------------------------------------------
var scrollToElement = function(elementId){
  $('html, body').animate({scrollTop: $("#" + elementId).offset().top}, 2000);
}
//----------------------------------------------------------------------------
var copyUrlToClipboard = function(){
  var url = window.location.href;
  var temp = $("<input>");
  $("body").append($temp);
  temp.val(url).select();
  document.execCommand("copy");
  temp.remove();
}
//----------------------------------------------------------------------------
// Login button logic
//----------------------------------------------------------------------------
$('button[name="fl-submit"]').click(function(e){
  var button = $(this);

  var login = $('input[name="fl-login"]');
  var password = $('input[name="fl-password"]');

  var err_login = $('#form-login-error');
  var err_password = $('#form-password-error');
  var err_validation = $('#form-Validate-error');

  button.attr('disabled', true);

  err_login.slideUp(0);
  err_password.slideUp(0);
  err_validation.slideUp(0);

  if (login.val().trim() == ''){
    err_login.text('Pole "Login" nie może być puste');
    err_login.slideDown('fast');
  }

  if (password.val().trim() == ''){
    err_password.text('Pole "Hasło" nie może być puste');
    err_password.slideDown('fast');
  }

  var request = {
    'fl-login' : login.val().trim(),
    'fl-password' : password.val().trim()
  };

  //console.log(request);
  var response;

  $.ajax({
    url: "api/v1/auth.php",
    type: "POST",
    //contentType: "json",
    data: request,
    success: function(result){
      console.log(result);
      response = JSON.parse(result);

      if (response.httpCode == 200 && response.requestStatus == 'success'){
        window.location.replace('index.php');
      } else {
        err_validation.text(response.message);
        err_validation.slideDown('fast');
      }
    },
    error : function(result, status) {
      err_validation.text(JSON.parse(result.responseText).message);
      err_validation.slideDown('fast');
    },
    complete: function(result, status) {
      button.attr('disabled', false);
    }
  });

  e.preventDefault();
});
//----------------------------------------------------------------------------
// Toggle roles reports
//----------------------------------------------------------------------------
$('.nav-role h2').click(function(){
  $(this).parent().children('.nav-links').slideToggle('fast');
//nav-links
});
//----------------------------------------------------------------------------
// Fix layout properties
//----------------------------------------------------------------------------
var top_panel = $('#top-panel');
var left_panel = $('#left-panel');

var fixedHeight = window.innerHeight - top_panel.outerHeight(true);

left_panel.css('min-height', fixedHeight + 'px');
//----------------------------------------------------------------------------
*/

});
