<?php
class Database
{
  //----------------------------------------------------------------------------
  private $db_host = 'localhost';
  private $db_user = 'root';
  private $db_pass = '';
  private $db_name = 'ticket_db';
  private $db_connection_string = 'mysql:host%s;dbname=%s;charset=utf8';

  private $db_handle = null;
  //----------------------------------------------------------------------------
  public function createConnection ()
  {
    $this->db_connection_string = sprintf($this->db_connection_string, $this->db_host, $this->db_name);

    try
    {
      $this->db_handle = new PDO($this->db_connection_string,
                                 $this->db_user,
                                 $this->db_pass);

      $this->db_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e)
    {
      echo '<pre>'.$e->getMessage().'</pre>';
      die();
    }
  }
  //----------------------------------------------------------------------------
  public function connect ()
  {
    $this->createConnection();

    return $this->db_handle;
  }
  //----------------------------------------------------------------------------
}
?>
