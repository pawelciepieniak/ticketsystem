<?php
require_once 'Database.class.php';

class Queue extends Database
{
  //----------------------------------------------------------------------------
  private $id;
  private $name;
  //----------------------------------------------------------------------------
  public function __construct ($id, $name)
  {
    $this->id = $id;
    $this->name = $name;
  }
  //----------------------------------------------------------------------------
  public function getID ()
  {
    return $this->id;
  }
  //----------------------------------------------------------------------------
  public function getName ()
  {
    return $this->name;
  }
  //----------------------------------------------------------------------------
  public function add ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      INSERT INTO ticket_db.ticket_queues VALUES(
        :id,
        :name
      )
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_INT);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);

    if (!$statement->execute())
    {
      echo '<pre>';
      var_dump($statement->errorInfo());
      echo '</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function update ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      UPDATE ticket_db.ticket_queues
      SET
        queue_name = :name
      WHERE queue_id = :id
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_INT);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);

    if (!$statement->execute())
    {
      echo '<pre>';
      var_dump($statement->errorInfo());
      echo '</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function delete ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('DELETE FROM ticket_db.ticket_queues WHERE queue_id = :queue_id');
    $statement->bindValue(':queue_id', $this->getID(), PDO::PARAM_INT);

    if (!$statement->execute())
    {
      echo '<pre>';
      var_dump($statement->errorInfo());
      echo '</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
}
?>
