<?php
class App
{
  //----------------------------------------------------------------------------
  public function checkAdminSession ()
  {
    if (empty($_SESSION['admin_id']) || empty($_SESSION['admin_auth']))
      return false;
    return true;
  }
  //----------------------------------------------------------------------------
  public function createAdminSession ($id)
  {
    $_SESSION['admin_id'] = $id;
    $_SESSION['admin_auth'] = true;
  }
  //----------------------------------------------------------------------------
  public function destroyAdminSession()
  {
    unset($_SESSION['admin_id']);
    unset($_SESSION['admin_auth']);
    session_destroy();
  }
  //----------------------------------------------------------------------------
  public function checkSession ()
  {
    if (empty($_SESSION['user_id']) || empty($_SESSION['user_auth']))
      return false;
    return true;
  }
  //----------------------------------------------------------------------------
  public function createSession ($id)
  {
    $_SESSION['user_id'] = $id;
    $_SESSION['user_auth'] = true;
  }
  //----------------------------------------------------------------------------
  public function destroySession()
  {
    unset($_SESSION['user_id']);
    unset($_SESSION['user_auth']);
    session_destroy();
  }
  //----------------------------------------------------------------------------
  public function hashPassword ($value)
  {
    return password_hash($value, PASSWORD_DEFAULT );
  }
  //----------------------------------------------------------------------------
  public function redirect ($url)
  {
    header('Location: '.$url);
    exit();
  }
  //----------------------------------------------------------------------------
  public function debug ($input)
  {
    echo '<pre>';
      var_dump($input);
    echo '</pre>';

  }
  //----------------------------------------------------------------------------
  public function generatePassword ()
  {
    $password_letters = '1234567890!@#$%^&abcedfghijklmnopqrstuwxyzABCDEFGHIJKLMNOPRSTUQWXYZ';
    $password_length = 8;
    $output = null;

    for ($i = 0; $i < $password_length; $i++)
    {
      $output .= $password_letters[rand(0, (strlen($password_letters) - 1))];
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function parse_size ($size)
  {
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
    $size = preg_replace('/[^0-9\.]/', '', $size);

    if ($unit)
    {
      return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    }
    else
    {
      return round($size);
    }
  }
  //----------------------------------------------------------------------------
  function file_upload_max_size () {
    $max_size = -1;

    if ($max_size < 0)
    {
      $post_max_size = $this->parse_size(ini_get('post_max_size'));

      if ($post_max_size > 0)
      {
        $max_size = $post_max_size;
      }

      $upload_max = $this->parse_size(ini_get('upload_max_filesize'));

      if ($upload_max > 0 && $upload_max < $max_size)
      {
        $max_size = $upload_max;
      }
    }
    return $max_size;
  }
  //----------------------------------------------------------------------------
  public function closeResolvedTickets($Database)
  {
    $DatabaseHandler = $Database->connect();
    $statement = $DatabaseHandler->prepare('UPDATE ticket_db.tickets SET ticket_status_id = 6 WHERE ticket_status_id = 5 AND ticket_create_date = DATE_ADD(CURDATE(), INTERVAL -7 DAY)');
    $statement->execute();
  }
  //----------------------------------------------------------------------------
}
?>
