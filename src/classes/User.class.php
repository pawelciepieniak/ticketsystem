<?php
require_once 'Database.class.php';

class User extends Database
{
  //----------------------------------------------------------------------------
  private $id;
  private $name;
  private $lastname;
  private $email;
  private $password;
  private $phone_number;
  private $account_status;
  private $queue_id;
  private $queue_name;
  private $reg_date;
  private $last_online_date;
  //----------------------------------------------------------------------------
  public function __construct ($id,
                              $name,
                              $lastname,
                              $email,
                              $password,
                              $phone_number,
                              $account_status,
                              $queue_id,
                              $queue_name,
                              $reg_date,
                              $last_online_date)
  {
    $this->id = $id;
    $this->name = $name;
    $this->lastname = $lastname;
    $this->email = $email;
    $this->password = $password;
    $this->phone_number = $phone_number;
    $this->account_status = $account_status;
    $this->queue_id = $queue_id;
    $this->queue_name = $queue_name;
    $this->reg_date = $reg_date;
    $this->last_online_date = $last_online_date;
  }
  //----------------------------------------------------------------------------
  public function getID ()
  {
    return $this->id;
  }
  //----------------------------------------------------------------------------
  public function getName ()
  {
    return $this->name;
  }
  //----------------------------------------------------------------------------
  public function getLastname ()
  {
    return $this->lastname;
  }
  //----------------------------------------------------------------------------
  public function getEmail ()
  {
    return $this->email;
  }
  //----------------------------------------------------------------------------
  public function getPassword ()
  {
    return $this->password;
  }
  //----------------------------------------------------------------------------
  public function getPhoneNumber ()
  {
    return $this->phone_number;
  }
  //----------------------------------------------------------------------------
  public function getAccountStatus ()
  {
    return $this->account_status;
  }
  //----------------------------------------------------------------------------
  public function getAccountStatusName ()
  {
    $output = null;

    if ($this->getAccountStatus() == 0) $output = 'Nieaktywne';
    if ($this->getAccountStatus() == 1) $output = 'Aktywne';

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getQueueID ()
  {
    return $this->queue_id;
  }
  //----------------------------------------------------------------------------
  public function getQueueName ()
  {
    return $this->queue_name;
  }
  //----------------------------------------------------------------------------
  public function getRegDate ()
  {
    return $this->reg_date;
  }
  //----------------------------------------------------------------------------
  public function getLastOnlineDate ()
  {
    return $this->last_online_date;
  }
  //----------------------------------------------------------------------------
  public function add ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      INSERT INTO ticket_db.users VALUES(
        :id,
        :name,
        :lastname,
        :email,
        :password,
        :phone_number,
        :account_status,
        :queue_id,
        :reg_date,
        :last_online_date
      )
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_STR);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);
    $statement->bindValue(':lastname', $this->getLastname(), PDO::PARAM_STR);
    $statement->bindValue(':email', $this->getEmail(), PDO::PARAM_STR);
    $statement->bindValue(':password', $this->getPassword(), PDO::PARAM_STR);
    $statement->bindValue(':phone_number', $this->getPhoneNumber(), PDO::PARAM_STR);
    $statement->bindValue(':account_status', $this->getAccountStatus(), PDO::PARAM_INT);
    $statement->bindValue(':queue_id', $this->getQueueID(), PDO::PARAM_INT);
    $statement->bindValue(':reg_date', $this->getRegDate(), PDO::PARAM_STR);
    $statement->bindValue(':last_online_date', $this->getLastOnlineDate(), PDO::PARAM_STR);

    if ($statement->execute())
    {
      // Wysyłam emaila z hasłem
      $x=0;
    }
    else
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function update ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      UPDATE ticket_db.users
      SET
        user_name = :name,
        user_lastname = :lastname,
        user_email = :email,
        user_phone_number = :phone_number,
        user_account_status = :account_status,
        user_queue_id  = :queue_id
      WHERE user_id = :id
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_STR);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);
    $statement->bindValue(':lastname', $this->getLastname(), PDO::PARAM_STR);
    $statement->bindValue(':email', $this->getEmail(), PDO::PARAM_STR);
    $statement->bindValue(':phone_number', $this->getPhoneNumber(), PDO::PARAM_STR);
    $statement->bindValue(':account_status', $this->getAccountStatus(), PDO::PARAM_INT);
    $statement->bindValue(':queue_id', $this->getQueueID(), PDO::PARAM_INT);

    if (!$statement->execute())
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function delete ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('DELETE FROM ticket_db.users WHERE user_id = :user_id');
    $statement->bindValue(':user_id', $this->getID(), PDO::PARAM_INT);

    if (!$statement->execute())
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function updateLastOnline ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'UPDATE ticket_db.users
       SET user_last_online_date = :user_last_online_date
       WHERE user_id = :user_id'
    );
    $statement->bindValue(':user_last_online_date', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $statement->bindValue(':user_id', $this->getID(), PDO::PARAM_INT);

    if (!$statement->execute())
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
}
?>
