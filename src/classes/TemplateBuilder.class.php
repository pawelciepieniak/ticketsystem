<?php
class TemplateBuilder
{
  //----------------------------------------------------------------------------
  private $template = null;
  //----------------------------------------------------------------------------
  public function prepare ($path)
  {
    $this->template = file_get_contents($path);
  }
  //----------------------------------------------------------------------------
  public function bind ($templateVar, $content)
  {
    $this->template = str_replace($templateVar, $content, $this->template);
  }
  //----------------------------------------------------------------------------
  public function render ()
  {
    return $this->template;
  }
  //----------------------------------------------------------------------------
}
?>
