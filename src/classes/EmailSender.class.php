<?php
require_once 'TemplateBuilder.class.php';

class EmailSender extends TemplateBuilder
{
  //----------------------------------------------------------------------------
  private $recipient;
  private $subject;
  private $message;
  private $headers;
  //----------------------------------------------------------------------------
  public function setRecipient ($value)
  {
    $this->recipient = $value;
  }
  //----------------------------------------------------------------------------
  public function getRecipient ()
  {
    return $this->recipient;
  }
  //----------------------------------------------------------------------------
  public function setSubject ($value)
  {
    $this->subject = $value;
  }
  //----------------------------------------------------------------------------
  public function getSubject ()
  {
    return $this->subject;
  }
  //----------------------------------------------------------------------------
  public function setMessage ($value)
  {
    $this->message = wordwrap($value);
  }
  //----------------------------------------------------------------------------
  public function getMessage ()
  {
    return $this->message;
  }
  //----------------------------------------------------------------------------
  public function setHeaders ()
  {
    $this->headers = "MIME-Version: 1.0" . "\r\n";
    $this->headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $this->headers .= 'From: <no-reply@ticketsystem.com>' . "\r\n";
  }
  //----------------------------------------------------------------------------
  public function getHeaders ()
  {
    return $this->headers;
  }
  //----------------------------------------------------------------------------
  public function send ()
  {
    if (!mail(
          $this->getRecipient(),
          $this->getSubject(),
          $this->getMessage(),
          $this->getHeaders()
        )) echo 'Dupa';
  }
  //----------------------------------------------------------------------------
}
?>
