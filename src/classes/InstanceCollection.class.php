<?php
require_once 'Database.class.php';

class InstanceCollection extends Database
{
  //----------------------------------------------------------------------------
  private $additionalParameters = null;
  //----------------------------------------------------------------------------
  public function setAdditionalParameters ($additionalParameters)
  {
    $this->additionalParameters = $additionalParameters;
  }
  //----------------------------------------------------------------------------
  public function getAdditionalParameters ()
  {
    return $this->additionalParameters;
  }
  //----------------------------------------------------------------------------
  public function getConfig ()
  {
    $PDO = $this->connect();
    $output = array();

    $statement = $PDO->prepare('SELECT * FROM ticket_db.app_config '.$this->getAdditionalParameters());
    $statement->execute();

    $fetcheData = $statement->fetch(PDO::FETCH_ASSOC);

    if (count($fetcheData) > 0)
    {
      foreach ($fetcheData as $key => $value)
      {
        $output[$key] = $value;
      }
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getQueuetList ()
  {
    $PDO = $this->connect();
    $output = array();

    $statement = $PDO->prepare('SELECT queue_id, queue_name FROM ticket_db.ticket_queues '.$this->getAdditionalParameters());
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, new Queue($row['queue_id'], $row['queue_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getQueueFlow ($queue_id)
  {
    $PDO = $this->connect();
    $output = array();

    $statement = $PDO->prepare('SELECT q.queue_id,
                                       q.queue_name,
                                       f.target_queue_id
                                  FROM ticket_db.ticket_queues q
                             LEFT JOIN ticket_db.queues_flow f
                                    ON q.queue_id = f.target_queue_id
                                   AND f.queue_id = :queue_id
                              ORDER BY queue_name
                                   ASC');

    $statement->bindValue(':queue_id', $queue_id, PDO::PARAM_INT);
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('queue_id' => $row['queue_id'], 'queue_name' => $row['queue_name'], 'target_queue_id' => $row['target_queue_id']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getProductList ()
  {
    $PDO = $this->connect();
    $output = array();

    $statement = $PDO->prepare('SELECT product_id, product_name FROM ticket_db.ticket_products '.$this->getAdditionalParameters());
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, new Product($row['product_id'], $row['product_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getUserList ()
  {
    $PDO = $this->connect();
    $output = array();

    $statement = $PDO->prepare('SELECT u.user_id,
                                       u.user_name,
                                       u.user_lastname,
                                       u.user_email,
                                       u.user_password,
                                       u.user_phone_number,
                                       u.user_account_status,
                                       u.user_queue_id,
                                       t.queue_name,
                                       u.user_reg_date,
                                       u.user_last_online_date
                                  FROM ticket_db.users u
                             LEFT JOIN ticket_db.ticket_queues t
                                    ON u.user_queue_id = t.queue_id '.$this->getAdditionalParameters());
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, new User(
        $row['user_id'],
        $row['user_name'],
        $row['user_lastname'],
        $row['user_email'],
        $row['user_password'],
        $row['user_phone_number'],
        $row['user_account_status'],
        $row['user_queue_id'],
        $row['queue_name'],
        $row['user_reg_date'],
        $row['user_last_online_date']
      ));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
}
?>
