<?php
require_once 'Database.class.php';

class Product extends Database
{
  //----------------------------------------------------------------------------
  private $id;
  private $name;
  //----------------------------------------------------------------------------
  public function __construct ($id, $name)
  {
    $this->id = $id;
    $this->name = $name;
  }
  //----------------------------------------------------------------------------
  public function getID ()
  {
    return $this->id;
  }
  //----------------------------------------------------------------------------
  public function getName ()
  {
    return $this->name;
  }
  //----------------------------------------------------------------------------
  public function add ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      INSERT INTO ticket_db.ticket_products VALUES(
        :id,
        :name
      )
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_INT);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);

    if (!$statement->execute())
    {
      echo '<pre>';
      var_dump($statement->errorInfo());
      echo '</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function update ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('
      UPDATE ticket_db.ticket_products
      SET
        product_name = :name
      WHERE product_id = :id
    ');

    $statement->bindValue(':id', $this->getID(), PDO::PARAM_INT);
    $statement->bindValue(':name', $this->getName(), PDO::PARAM_STR);

    if (!$statement->execute())
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
  public function delete ()
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('DELETE FROM ticket_db.ticket_products WHERE product_id = :product_id');
    $statement->bindValue(':product_id', $this->getID(), PDO::PARAM_INT);

    if (!$statement->execute())
    {
      echo '<pre>'.$statement->errorInfo().'</pre>';
      exit();
    }
  }
  //----------------------------------------------------------------------------
}
?>
