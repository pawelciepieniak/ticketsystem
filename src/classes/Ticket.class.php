<?php
require_once 'Database.class.php';

class Ticket extends Database
{
  //----------------------------------------------------------------------------
  public function ticketExists ($ticket_id)
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT *
        FROM ticket_db.tickets
        WHERE ticket_id = :ticket_id'
    );
    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    if ($statement->rowCount() > 0)
      return true;
    return false;
  }
  //----------------------------------------------------------------------------
  public function addTime($user_id, $ticket_id, $time_amount)
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'INSERT INTO ticket_db.ticket_work_time(ticket_id, user_id, time_spend, event_date) VALUES(:ticket_id, :user_id, :time_spend, :event_date)');
    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $statement->bindValue(':time_spend', $time_amount, PDO::PARAM_INT);
    $statement->bindValue(':event_date', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $statement->execute();
  }
  //----------------------------------------------------------------------------
  public function removeSubscriber ($ticket_id, $subscriber_id)
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('DELETE FROM ticket_db.ticket_subscribers WHERE ticket_id = :ticket_id AND user_id = :user_id');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':user_id', $subscriber_id, PDO::PARAM_INT);
    $statement->execute();
  }
  //----------------------------------------------------------------------------
  public function addSubscriber ($ticket_id, $inviter_id, $subscriber_id)
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('INSERT INTO ticket_db.ticket_subscribers VALUES(:ticket_id, :user_id, :inviter_id, :event_date)');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':user_id', $subscriber_id, PDO::PARAM_INT);
    $statement->bindValue(':inviter_id', $inviter_id, PDO::PARAM_INT);
    $statement->bindValue(':event_date', date('Y-m-d H:i:s'), PDO::PARAM_STR);
    $statement->execute();
  }
  //----------------------------------------------------------------------------
  public function checkSubscriber ($ticket_id, $subscriber_id)
  {
    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT * FROM ticket_db.ticket_subscribers WHERE ticket_id = :ticket_id AND user_id = :user_id');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->bindValue(':user_id', $subscriber_id, PDO::PARAM_INT);
    $statement->execute();

    if ($statement->rowCount() > 0)
      return true;
    return false;
  }
  //----------------------------------------------------------------------------
  public function getQueueList ($user_queue_id)
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT q.queue_id,
              q.queue_name
        FROM ticket_db.ticket_queues q
        JOIN ticket_db.queues_flow f
          ON q.queue_id = f.target_queue_id
         AND f.queue_id = :queue_id
    ORDER BY q.queue_name
        ASC'
    );
    $statement->bindValue(':queue_id', $user_queue_id, PDO::PARAM_INT);
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('queue_id' => $row['queue_id'], 'queue_name' => $row['queue_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getFullQueueList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT q.queue_id,
              q.queue_name
        FROM ticket_db.ticket_queues q
    ORDER BY q.queue_name
        ASC'
    );
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('queue_id' => $row['queue_id'], 'queue_name' => $row['queue_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getQueueUserList ($queue_id)
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT
              user_id,
              user_name,
              user_lastname,
              user_email,
              user_phone_number
          FROM
              ticket_db.users
          WHERE
              user_queue_id = :queue_id
          ORDER BY
              user_lastname ASC,
              user_name ASC'
    );
    $statement->bindValue(':queue_id', $queue_id, PDO::PARAM_INT);
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('user_id' => $row['user_id'], 'user_name' => $row['user_name'], 'user_lastname' => $row['user_lastname'], 'user_email' => $row['user_email'], 'user_phone_number' => $row['user_phone_number']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getFullQueueUserList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT
              user_id,
              user_name,
              user_lastname,
              user_email,
              user_phone_number
          FROM
              ticket_db.users
          WHERE
              user_queue_id <> 1
          ORDER BY
              user_lastname ASC,
              user_name ASC'
    );
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('user_id' => $row['user_id'], 'user_name' => $row['user_name'], 'user_lastname' => $row['user_lastname'], 'user_email' => $row['user_email'], 'user_phone_number' => $row['user_phone_number']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getProductList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT product_id, product_name FROM ticket_db.ticket_products ORDER BY product_name ASC');
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('product_id' => $row['product_id'], 'product_name' => $row['product_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getStatusList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT ticket_status_id, ticket_status_name FROM ticket_db.ticket_statuses ORDER BY ticket_status_id ASC');
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('ticket_status_id' => $row['ticket_status_id'], 'ticket_status_name' => $row['ticket_status_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getStatusNameByID ($status)
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT ticket_status_name FROM ticket_db.ticket_statuses WHERE ticket_status_id = :ticket_status_id');
    $statement->bindValue(':ticket_status_id', $status, PDO::PARAM_INT);
    $statement->execute();

    $row = $statement->fetch(PDO::FETCH_ASSOC);
    $output = $row['ticket_status_name'];

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getPriorityList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT priority_id, priority_name FROM ticket_db.ticket_priorities ORDER BY priority_id ASC');
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('priority_id' => $row['priority_id'], 'priority_name' => $row['priority_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getEnvList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT env_id, env_name FROM ticket_db.ticket_envs ORDER BY env_id DESC');
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('env_id' => $row['env_id'], 'env_name' => $row['env_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getImpactList ()
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT impact_id, impact_name FROM ticket_db.ticket_impacts ORDER BY impact_id ASC');
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output, array('impact_id' => $row['impact_id'], 'impact_name' => $row['impact_name']));
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getSubscriberList ($user_queue_id, $user_id)
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare(
      'SELECT u.user_id,
              u.user_name,
              u.user_lastname,
              u.user_email,
              u.user_phone_number
        FROM ticket_db.users u
        WHERE u.user_queue_id = :user_queue_id
         AND u.user_id <> :user_id');

    $statement->bindValue(':user_queue_id', $user_queue_id, PDO::PARAM_INT);
    $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $statement->execute();

    while ($row = $statement->fetch(PDO::FETCH_ASSOC))
    {
      array_push($output,
                array('user_id' => $row['user_id'],
                      'user_name' => $row['user_name'],
                      'user_lastname' => $row['user_lastname'],
                      'user_email' => $row['user_email'],
                      'user_phone_number' => $row['user_phone_number']
                )
      );
    }

    return $output;
  }
  //----------------------------------------------------------------------------
  public function getSLAList ()
  {
    return array('standard', 'silver', 'gold');
  }
  //----------------------------------------------------------------------------
  public function getSLA ($env, $impact, $priority)
  {
    $stanard = 'STANDARD';
    $silver = 'SILVER';
    $gold = 'GOLD';

    $sla = null;
    /*--------------------------------------------------------------------------
      ENV
      --------------------------------------------------------------------------
      1 = Test
      2 = UAT
      3 = Production
      --------------------------------------------------------------------------
      IMPACT
      --------------------------------------------------------------------------
      1	No impact
      2	User
      3 Team/Departament
      4 Activity
      --------------------------------------------------------------------------
      PRIORITY
      --------------------------------------------------------------------------
      1	Niski
      2	Normalny
      3	Wysoki
      4	Krytyczny
    --------------------------------------------------------------------------*/
    if ($env == 3)
    {
      if ($impact == 1 && $priority == 1) $sla = $stanard;
      if ($impact == 1 && $priority == 2) $sla = $stanard;
      if ($impact == 1 && $priority == 3) $sla = $stanard;
      if ($impact == 1 && $priority == 4) $sla = $silver;

      if ($impact == 2 && $priority == 1) $sla = $stanard;
      if ($impact == 2 && $priority == 2) $sla = $stanard;
      if ($impact == 2 && $priority == 3) $sla = $silver;
      if ($impact == 2 && $priority == 4) $sla = $gold;

      if ($impact == 3 && $priority == 1) $sla = $stanard;
      if ($impact == 3 && $priority == 2) $sla = $stanard;
      if ($impact == 3 && $priority == 3) $sla = $silver;
      if ($impact == 3 && $priority == 4) $sla = $gold;

      if ($impact == 4 && $priority == 1) $sla = $stanard;
      if ($impact == 4 && $priority == 2) $sla = $silver;
      if ($impact == 4 && $priority == 3) $sla = $gold;
      if ($impact == 4 && $priority == 4) $sla = $gold;
    }

    if ($env == 2)
    {
      if ($impact == 1 && $priority == 1) $sla = $stanard;
      if ($impact == 1 && $priority == 2) $sla = $stanard;
      if ($impact == 1 && $priority == 3) $sla = $stanard;
      if ($impact == 1 && $priority == 4) $sla = $stanard;

      if ($impact == 2 && $priority == 1) $sla = $stanard;
      if ($impact == 2 && $priority == 2) $sla = $stanard;
      if ($impact == 2 && $priority == 3) $sla = $stanard;
      if ($impact == 2 && $priority == 4) $sla = $silver;

      if ($impact == 3 && $priority == 1) $sla = $stanard;
      if ($impact == 3 && $priority == 2) $sla = $stanard;
      if ($impact == 3 && $priority == 3) $sla = $stanard;
      if ($impact == 3 && $priority == 4) $sla = $silver;

      if ($impact == 4 && $priority == 1) $sla = $stanard;
      if ($impact == 4 && $priority == 2) $sla = $stanard;
      if ($impact == 4 && $priority == 3) $sla = $stanard;
      if ($impact == 4 && $priority == 4) $sla = $silver;
    }

    if ($env == 1)
    {
      if ($impact == 1 && $priority == 1) $sla = $stanard;
      if ($impact == 1 && $priority == 2) $sla = $stanard;
      if ($impact == 1 && $priority == 3) $sla = $stanard;
      if ($impact == 1 && $priority == 4) $sla = $stanard;

      if ($impact == 2 && $priority == 1) $sla = $stanard;
      if ($impact == 2 && $priority == 2) $sla = $stanard;
      if ($impact == 2 && $priority == 3) $sla = $stanard;
      if ($impact == 2 && $priority == 4) $sla = $stanard;

      if ($impact == 3 && $priority == 1) $sla = $stanard;
      if ($impact == 3 && $priority == 2) $sla = $stanard;
      if ($impact == 3 && $priority == 3) $sla = $stanard;
      if ($impact == 3 && $priority == 4) $sla = $stanard;

      if ($impact == 4 && $priority == 1) $sla = $stanard;
      if ($impact == 4 && $priority == 2) $sla = $stanard;
      if ($impact == 4 && $priority == 3) $sla = $stanard;
      if ($impact == 4 && $priority == 4) $sla = $stanard;
    }

    return $sla;
  }
  //----------------------------------------------------------------------------
  public function loadPreview ($ticket_id, $user_id)
  {
    $output = array();

    $Database = new Database();
    $DatabaseHandler = $Database->connect();

    $statement = $DatabaseHandler->prepare('SELECT
                                                    t.ticket_sla,
                                                    t.ticket_status_id,
                                                    ts.ticket_status_name,
                                                    t.ticket_queue_id,
                                                    tq.queue_name,
                                                    t.ticket_owner_id AS owner_id,
                                                    towner.user_name AS owner_name,
                                                    towner.user_lastname AS owner_lastname,
                                                    towner.user_email AS owner_email,
                                                    towner.user_phone_number AS owner_phone_number,
                                                    t.ticket_requestor_id AS requestor_id,
                                                    requestor.user_name AS requestor_name,
                                                    requestor.user_lastname AS requestor_lastname,
                                                    requestor.user_email AS requestor_email,
                                                    requestor.user_phone_number AS requestor_phone_number,
                                                    t.ticket_product_id,
                                                    t.ticket_env_id,
                                                    te.env_name,
                                                    t.ticket_priority_id,
                                                    tp.priority_name,
                                                    t.ticket_impact_id,
                                                    ti.impact_name,
                                                    t.ticket_parent_id,
                                                    t.ticket_external_id,
                                                    t.ticket_create_date,
                                                    t.ticket_create_time,
                                                    t.ticket_resolve_date,
                                                    t.ticket_resolve_time,
                                                    t.ticket_headline,
                                                    t.ticket_description
                                                FROM
                                                    ticket_db.tickets t
                                                JOIN ticket_db.ticket_statuses ts ON
                                                    ts.ticket_status_id = t.ticket_status_id
                                                JOIN ticket_db.ticket_queues tq ON
                                                    tq.queue_id = t.ticket_queue_id
                                                JOIN ticket_db.users requestor ON
                                                    requestor.user_id = t.ticket_requestor_id
                                                LEFT JOIN ticket_db.users towner ON
                                                    towner.user_id = t.ticket_owner_id
                                                JOIN ticket_db.ticket_envs te ON
                                                    te.env_id = t.ticket_env_id
                                                JOIN ticket_db.ticket_priorities tp ON
                                                    tp.priority_id = t.ticket_priority_id
                                                JOIN ticket_db.ticket_impacts ti ON
                                                    ti.impact_id = t.ticket_impact_id
                                                WHERE
                                                    t.ticket_id = :ticket_id');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['ticket'] = $statement->fetch(PDO::FETCH_ASSOC);

    $statement = $DatabaseHandler->prepare('SELECT
                                                    ta.attachment_id,
                                                    ta.attachment_ticket_id,
                                                    ta.attachment_comment_id,
                                                    ta.attachment_upload_date,
                                                    ta.attachment_mime_type,
                                                    ta.attachment_source_path,
                                                    ta.attachment_owner_id,
                                                    u.user_name,
                                                    u.user_lastname
                                                FROM
                                                    ticket_db.ticket_attachments ta
                                                JOIN ticket_db.users u ON
                                                    u.user_id = ta.attachment_owner_id
                                                WHERE
                                                    ta.attachment_ticket_id = :ticket_id
                                                ORDER BY
                                                    ta.attachment_upload_date
                                                DESC');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['attachments'] = array();
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) array_push($output['attachments'], $row);

    $statement = $DatabaseHandler->prepare('SELECT
                                                    tc.comment_id,
                                                    tc.comment_ticket_id,
                                                    tc.comment_description,
                                                    tc.comment_author_id,
                                                    u.user_name,
                                                    u.user_lastname,
                                                    tc.comment_create_date
                                                FROM
                                                    ticket_db.ticket_comments tc
                                                JOIN ticket_db.users u ON u.user_id = tc.comment_author_id
                                                WHERE
                                                    tc.comment_ticket_id = :ticket_id
                                                ORDER BY
                                                    tc.comment_create_date
                                                DESC');


    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['comments'] = array();
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) array_push($output['comments'], $row);

    $statement = $DatabaseHandler->prepare('SELECT
                                                    ael.event_id,
                                                    ael.event_ticket_id,
                                                    ael.event_trigger_id,
                                                    u.user_name,
                                                    u.user_lastname,
                                                    ael.event_type,
                                                    ael.event_date,
                                                    ael.event_details
                                                FROM
                                                    ticket_db.app_event_log ael
                                                LEFT JOIN ticket_db.users u ON u.user_id = ael.event_trigger_id
                                                WHERE
                                                    ael.event_ticket_id = :ticket_id
                                                ORDER BY
                                                	ael.event_date DESC');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['events'] = array();
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) array_push($output['events'], $row);

    $statement = $DatabaseHandler->prepare('SELECT
                                                    ts.ticket_id,
                                                    ts.user_id,
                                                    u.user_name,
                                                    u.user_lastname,
                                                    u.user_phone_number,
                                                    u.user_email
                                                FROM
                                                    ticket_db.ticket_subscribers ts
                                                JOIN ticket_db.users u ON ts.user_id = u.user_id
                                                WHERE
                                                    ts.ticket_id = :ticket_id
                                                ORDER BY
                                                    ts.event_date
                                                DESC');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['subscribers'] = array();
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) array_push($output['subscribers'], $row);

    $statement = $DatabaseHandler->prepare('SELECT
                                                    u.user_name,
                                                    u.user_lastname,
                                                    twt.event_date,
                                                    twt.time_spend
                                                FROM
                                                    ticket_db.ticket_work_time twt
                                                JOIN ticket_db.users u ON
                                                    u.user_id = twt.user_id
                                                WHERE
                                                    twt.ticket_id = :ticket_id
                                                ORDER BY
                                                    twt.event_date
                                                DESC');

    $statement->bindValue(':ticket_id', $ticket_id, PDO::PARAM_INT);
    $statement->execute();

    $output['time'] = array();
    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) array_push($output['time'], $row);

    return $output;
  }
  //----------------------------------------------------------------------------
}
?>
