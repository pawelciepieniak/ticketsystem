<?php
  require_once 'src/includes/config.php';
  #-----------------------------------------------------------------------------
  # Check session
  #-----------------------------------------------------------------------------
  if ($App->checkSession() == false) $App->redirect('login.php');
  #-----------------------------------------------------------------------------
  # Prepare data
  #-----------------------------------------------------------------------------
  $InstanceCollection->setAdditionalParameters('WHERE user_id = '.$_SESSION['user_id']);
  $User = $InstanceCollection->getUserList()[0];
  #-----------------------------------------------------------------------------
  # Prepare view
  #-----------------------------------------------------------------------------
  $uTemplate = new TemplateBuilder();
  $uTemplate->prepare('src/templates/user-profile.html');
  $uTemplate->bind('{{user-name}}', $User->getName());
  $uTemplate->bind('{{user-lastname}}', $User->getLastname());
  $uTemplate->bind('{{user-email}}', $User->getEmail());
  $uTemplate->bind('{{user-phone-number}}', $User->getPhoneNumber());
  $uTemplate->bind('{{user-account-status}}', $User->getAccountStatusName());
  $uTemplate->bind('{{user-queue}}', $User->getQueueName());
  $uTemplate->bind('{{user-reg-date}}', $User->getRegDate());

  $mTemplate->bind('{{page-title}}', PAGE_TITLE_USER_PROFILE);
  $mTemplate->bind('{{page-content}}', $uTemplate->render());

  echo $tPanel->render();
  echo $mTemplate->render();
?>
